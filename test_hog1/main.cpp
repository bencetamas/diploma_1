#include <opencv2/opencv.hpp>
#include "opencv2/imgcodecs.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include <iostream>
#include <math.h>
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <dirent.h>
#include <queue>

using namespace cv;
using namespace std;

string mainWinName="Main";

class Selection {
      int x1,y1,x2,y2;
      Mat img;


      void addToSelection(vector<Rect>& rects, Rect toBeAdded) {
         if (toBeAdded.height < 30 || toBeAdded.width < 30) 
            return;
         rects.push_back(toBeAdded);
      }
   public:
      Selection(Mat _img) :
        x1(-1),
        y1(-1),
        x2(-1),
        y2(-1),
        img(_img) {
      }

      Selection(const Selection& s) :
         x1(s.x1),
         x2(s.x2),
         y1(s.y1),
         y2(s.y2) {
      }

      ~Selection() {
         img.release();
      }

      bool isValid() {
         return x1>=0 && x2>=0 && y1 >=0 && y2>=0;
      }

      Point getTopLeft() {
         return Point(x1,y1);
      }

      Point getWidthHeight() {
         return Point(x2-x1,y2-y1);
      }

      void setTopLeft(int x, int y) {
         x1=x;
         y1=y;
      }

      void setBottomRight(int x, int y) {
         x2=x;
         y2=y;
         if (x1>x2) {
            int tmp=x1;
            x1=x2;
            x2=tmp;
         }
         if (y1>y2) {
            int tmp=y1;
            y1=y2;
            y2=tmp;
         }
      }
 
      Rect getSelection() {
         return Rect(Point(x1,y1),Point(x2,y2));
      }

      vector<Rect> getNonSelections() {
         vector<Rect> result;
         addToSelection(result,Rect(Point(0,0),Point(x1-1,img.size().height-1)));
         addToSelection(result,Rect(Point(0,0),Point(img.size().width-1,y1-1)));
         addToSelection(result,Rect(Point(x2+1,0),Point(img.size().width-1,img.size().height-1)));
         addToSelection(result,Rect(Point(0,y2+1),Point(img.size().width-1,img.size().height-1)));
         return result;
      }

      Mat getImage() {
         Mat tmp;
         img.copyTo(tmp);
         if (isValid()) 
            rectangle(tmp,getSelection(),Scalar(0,0,255),2);
         return tmp;
      }
};

Mat dummy,img;
Selection sel1(dummy),sel2(dummy);

void mouseClick(int event, int x, int y, int flags, void* userdata) {
   int ws=34;
   if (event == EVENT_LBUTTONDOWN) {
      if (sel1.isValid() && sel2.isValid()) {
         sel1=Selection(img);
         sel2=Selection(img);
         sel1.setTopLeft(x,y);
         sel1.setBottomRight(x+ws,y+ws);
      } else if (sel1.isValid()) {
         sel2.setTopLeft(x,y);
         sel2.setBottomRight(x+ws,y+ws);
      } else {
         sel1.setTopLeft(x,y);
         sel1.setBottomRight(x+ws,y+ws);
      }
   } /*else if (event == EVENT_LBUTTONUP) {
      if (!sel1.isValid()) {
         sel1.setBottomRight(x,y);
      } else {
         sel2.setBottomRight(x,y);
      }

   }*/
}

int main(int argc, char** argv )
{
    img=imread(argv[1]);
    sel1=Selection(img);
    sel2=Selection(img);

    namedWindow(mainWinName);
    setMouseCallback(mainWinName,mouseClick,NULL);

    Mat tmp;
    while (true) {
       img.copyTo(tmp);
       if (sel1.isValid())
          rectangle(tmp,sel1.getSelection(),Scalar(255,0,0),1);
       if (sel2.isValid())
          rectangle(tmp,sel2.getSelection(),Scalar(0,0,255),1);
       if (sel1.isValid() && sel2.isValid()) {
          Mat tmp1=img(sel1.getSelection());
          Mat tmp2=img(sel2.getSelection());
          HOGDescriptor hog;
          hog.winSize=Size(16,16);
          hog.blockSize=Size(16,16);
          hog.cellSize=Size(4,4);
          vector<float> desc1,desc2;
          hog.compute(tmp1,desc1);
          hog.compute(tmp2,desc2);
cout << "Sizes:" << desc1.size() << "," << desc2.size() << endl;
          double dist=0;
          for (int i=0 ; i< desc1.size(); i++) {
             dist+=abs(desc1[i]-desc2[i]);
          }
cout << "Distance=" << dist << endl;

         sel1=Selection(img);
         sel2=Selection(img);
       }
       imshow(mainWinName,tmp);
       waitKey(50);
    }

    return 0;
}
