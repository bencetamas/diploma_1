#ifndef __ALGORITHM_VIDEO_HPP__
#define __ALGORITHM_VIDEO_HPP__

#include <opencv2/opencv.hpp>
#include <string>
#include <stdio.h>
#include "../test_program1/algorithms_base.hpp"

using namespace std;
using namespace cv;

namespace my {

class VideoAlgorithm: public Algorithm {
   public:
      virtual Mat convert(Mat prev, Mat curr)=0;
 
      Mat convert(Mat img) {
         cout << "Wrong function invocation. <algorithms_video.hpp>";
         return Mat();
      }
};

}
#endif
