#ifndef __HAARTEST_HPP__
#define __HAARTEST_HPP__

#include <opencv2/opencv.hpp>
#include "opencv2/imgcodecs.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include <iostream>
#include <math.h>
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <dirent.h>
#include <queue>

#include "../algorithms_video.hpp"

using namespace std;
using namespace cv;

namespace my {

class HaarTest : public VideoAlgorithm {
      Param<int> dummy;
   public:
      HaarTest() :
         dummy("dummy",1,1,1,10) {
      }

      Mat convert(Mat prev,Mat curr) {
         Mat result;
         curr.copyTo(result);

         Mat tmp1,tmp2;
         cvtColor(prev,tmp1,CV_BGR2GRAY);
         cvtColor(curr,tmp2,CV_BGR2GRAY);
         prev=tmp1;
         curr=tmp2;

             // Load Face cascade (.xml file)
    CascadeClassifier face_cascade;
    face_cascade.load( "/usr/local/share/OpenCV/haarcascades/haarcascade_profileface.xml" );
 cout << "1" << endl;
    // Detect faces
    std::vector<Rect> faces;
    face_cascade.detectMultiScale( curr, faces, 1.1, 2, 0|CV_HAAR_SCALE_IMAGE, Size(10, 10) );
  cout << "2" << endl;
    // Draw circles on the detected faces
    for( int i = 0; i < faces.size(); i++ )
    {
        Point center( faces[i].x + faces[i].width*0.5, faces[i].y + faces[i].height*0.5 );
        ellipse( result, center, Size( faces[i].width*0.5, faces[i].height*0.5), 0, 0, 360, Scalar( 255, 0, 255 ), 4, 8, 0 );
    }

         return result;
      }

      vector<ParamBase*> getParams() {
         vector<ParamBase*> params;
         params.push_back(&dummy);
         return params;
      }

      string getName() {
         return "Haar test";
      }
};

}
#endif
