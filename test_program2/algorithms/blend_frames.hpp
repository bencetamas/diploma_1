#ifndef __BLENDFRAMES_HPP__
#define __BLENDFRAMES_HPP__

#include <opencv2/opencv.hpp>
#include "opencv2/imgcodecs.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include <iostream>
#include <math.h>
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <dirent.h>
#include <queue>

#include "../algorithms_video.hpp"

using namespace std;
using namespace cv;

namespace my {

class BlendFrames : public VideoAlgorithm {
      Param<int> number;
      Param<int> skip;
      vector<Mat> frames;
   public:
      BlendFrames() :
         number("# frames",6,1,0,50),
         skip("# skips",15,1,0,255) {
      }

      Mat convert(Mat prev,Mat curr) {
	 curr.convertTo(curr,CV_32FC3);
	 curr=curr/255.0;
         frames.push_back(curr);
	 while (frames.size() > number.getValue()*skip.getValue())
	    frames.erase(frames.begin());
	 Mat blend=Mat::zeros(curr.rows,curr.cols,curr.type());
	 cout << blend.rows << "," << curr.rows << endl;
	 int count=number.getValue()*skip.getValue() < frames.size() ? number.getValue() : frames.size()/skip.getValue();
         for (int i=0; i < count*skip.getValue(); i++) {
	    if (i%skip.getValue()==0) {
   	       blend=blend+frames[i];
	    }
	 }	
	 blend=blend*(1.0/count);
	 blend=blend*255.0;
	 blend.convertTo(blend,CV_8UC3);
	 cvtColor(blend,blend,CV_BGR2GRAY);
	 Mat edge;
	 Canny(blend,edge,100,200,3);
	 //normalize(blend,blend,0,1,NORM_MINMAX);
         return edge;
      }

      vector<ParamBase*> getParams() {
         vector<ParamBase*> params;
         params.push_back(&number);
         params.push_back(&skip);
         return params;
      }

      string getName() {
         return "blending frames";
      }
};

}
#endif
