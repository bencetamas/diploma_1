#ifndef __FARNEBACK_HPP__
#define __FARNEBACK_HPP__

#include <opencv2/opencv.hpp>
#include "opencv2/imgcodecs.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include <iostream>
#include <math.h>
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <dirent.h>
#include <queue>

#include "../algorithms_video.hpp"

using namespace std;
using namespace cv;

namespace my {

class Farneback : public VideoAlgorithm {
      Param<double> pyrScale;
      Param<int> level;
      Param<int> winSize;
      Param<int> iterations;
      Param<int> poly_n;      
      Param<double> poly_sigma;
      Param<double> arrowFactor;
   public:
      Farneback() :
         pyrScale("Scale",0.5,0.05,0,255),
         level("Level",3,1,0,255),
         winSize("Winsize",3,1,0,255),
         iterations("Iterations",3,1,0,255),
         poly_n("Poly n",5,1,0,255),
         poly_sigma("Poly sigma",1.1,0.05,0,255),
         arrowFactor("Arrow draw factor",2.0,0.5,0,200) {
      }

      Mat convert(Mat prev,Mat curr) {
         Mat tmp1,tmp2;
         cvtColor(prev,tmp1,CV_BGR2GRAY);
         cvtColor(curr,tmp2,CV_BGR2GRAY);
         prev=tmp1;
         curr=tmp2;

         Mat vel;
         //cvCalcOpticalFlowBM(prev,curr,blockSize.getValue(),shiftSize.getValue(),maxRange.getValue(),0,velx,vely);
         calcOpticalFlowFarneback(prev,curr,vel,pyrScale.getValue(),level.getValue(),winSize.getValue(),iterations.getValue(),poly_n.getValue(),poly_sigma.getValue(),0);
         Mat result;
         curr.copyTo(result);
         result=result*0.6;
         for (int x=0; x < curr.cols; x=x+7)
            for (int y=0; y < curr.rows; y=y+7) {
               double vx=vel.at<Vec2f>(y,x)[0];
               double vy=vel.at<Vec2f>(y,x)[1];
               arrowedLine(result,Point(x,y),Point(x+vx*arrowFactor.getValue(),y+vy*arrowFactor.getValue()),Scalar(0,0,255));
         }
         return result;
      }

      vector<ParamBase*> getParams() {
         vector<ParamBase*> params;
         params.push_back(&pyrScale);
         params.push_back(&level);
         params.push_back(&winSize);
         params.push_back(&iterations);
         params.push_back(&poly_n);
         params.push_back(&poly_sigma);
         params.push_back(&arrowFactor);
         return params;
      }

      string getName() {
         return "dense optical flow: Farneback";
      }
};

}
#endif
