#ifndef __CANNY_HPP__
#define __CANNY_HPP__

#include <opencv2/opencv.hpp>
#include "opencv2/imgcodecs.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include <iostream>
#include <math.h>
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <dirent.h>
#include <queue>

#include "../algorithms_video.hpp"

using namespace std;
using namespace cv;

namespace my {

class SimpleCanny : public VideoAlgorithm {
      Param<int> minTreshold;
      Param<int> maxTreshold;
   public:
      SimpleCanny() :
         minTreshold("Canny min. treshold",100,1,0,255),
         maxTreshold("Canny max. treshold",200,1,0,255) {
      }

      Mat convert(Mat prev,Mat original) {
         Mat result;
         Canny(original,result,minTreshold.getValue(),maxTreshold.getValue(),3);
         return result;
      }

      vector<ParamBase*> getParams() {
         vector<ParamBase*> params;
         params.push_back(&minTreshold);
         params.push_back(&maxTreshold);
         return params;
      }

      string getName() {
         return "'pure' Canny edge detection algorithm";
      }
};

}
#endif
