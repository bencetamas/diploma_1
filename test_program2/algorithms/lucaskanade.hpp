#ifndef __LUCASKANADE_HPP__
#define __LUCASKANADE_HPP__

#include <opencv2/opencv.hpp>
#include "opencv2/imgcodecs.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include <iostream>
#include <math.h>
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <dirent.h>
#include <queue>

#include "../algorithms_video.hpp"

using namespace std;
using namespace cv;

namespace my {

class LucasKanade : public VideoAlgorithm {
      Param<double> qualityLevel;
      Param<double> minDistance;
      Param<int> blockSize;
      Param<int> level;
      Param<int> winSize;
   public:
      LucasKanade() :
         qualityLevel("Quality level",0.01,0.002,0,2.0),
         minDistance("Minimum distance",5,1,0,200),
         blockSize("Block size",3,1,0,50),
         level("Max level",3,1,0,255),
         winSize("Winsize",21,1,0,255) {
      }

      Mat convert(Mat prev,Mat curr) {
         Mat result;
         curr.copyTo(result);

         Mat tmp1,tmp2;
         cvtColor(prev,tmp1,CV_BGR2GRAY);
         cvtColor(curr,tmp2,CV_BGR2GRAY);
         prev=tmp1;
         curr=tmp2;

         vector<Point2f> prevCorners, currCorners;
         goodFeaturesToTrack(prev,prevCorners,20000,qualityLevel.getValue(),minDistance.getValue(),noArray(),blockSize.getValue(),false);
         //goodFeaturesToTrack(curr,currCorners,20000,qualityLevel.getValue(),minDistance.getValue(),noArray(),blockSize.getValue(),false);

         vector<unsigned char> status;
         vector<float> err;
         Size s=Size(winSize.getValue(),winSize.getValue());
         calcOpticalFlowPyrLK(prev,curr,prevCorners,currCorners,status,err,s,level.getValue());

         result=result*0.6;
         for (int i=0; i<prevCorners.size(); i++)
           if (status[i]==1) {
               arrowedLine(result,prevCorners[i],currCorners[i],Scalar(0,0,255));
            }
         return result;
      }

      vector<ParamBase*> getParams() {
         vector<ParamBase*> params;
         params.push_back(&qualityLevel);
         params.push_back(&minDistance);
         params.push_back(&blockSize);
         params.push_back(&level);
         params.push_back(&winSize);
         return params;
      }

      string getName() {
         return "optical flow: lk";
      }
};

}
#endif
