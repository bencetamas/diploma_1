#include <opencv2/opencv.hpp>
#include "opencv2/imgcodecs.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include <iostream>
#include <math.h>
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <dirent.h>
#include <queue>

#include "algorithms_video.hpp"
#include "algorithms/canny.hpp"
#include "algorithms/farneback.hpp"
#include "algorithms/lucaskanade.hpp"
#include "algorithms/haar_test.hpp"
#include "algorithms/blend_frames.hpp"

#include "../test_program1/algorithms/compressx.hpp"
#include "../test_program1/algorithms/colorbank.hpp"

using namespace cv;
using namespace std;

class State {
   private:
      string MAIN_WIN;
      int MAIN_WINDOW_WIDTH;
      int MAIN_WINDOW_HEIGHT;
      double main_window_size;
      vector<string> files;
      vector<VideoCapture> captures;
      vector<Mat> prev;
      vector<Mat> curr;
      int selectedCapInd;
      int selectedParamInd;
      enum InfoType {INFO_HELP, INFO_PARAMS} info_page;
      bool useYUVspace;

      bool writeVideo;
      VideoWriter videoWriter;
      
      my::Algorithm* alg;
      my::AlgorithmApplication applyAlg;

      vector<string> getFileList(string dir) {
	DIR *dpdf;
	struct dirent *epdf;
 
	dpdf = opendir(dir.c_str());
	if (dpdf != NULL) {
	   vector<string> result;
	   int i=0;
	   while (epdf = readdir(dpdf)) {
	      result.push_back(string(dir)+string(epdf->d_name));
	      i++;  
	   }
	  
	   closedir(dpdf);
	   return result;
	}
	
	return vector<string>();
      }

      void loadCaptures(vector<string> _files) {
         captures.clear();
         prev.clear();
         curr.clear();
         for (int i=0; i < _files.size(); i++) {
	    VideoCapture tmp=VideoCapture(_files[i]);
	    if (tmp.isOpened()) {
              files.push_back(_files[i]);
	      captures.push_back(tmp);
              Mat p,c;
              tmp >> p;
              tmp >> c;
              prev.push_back(p);
              curr.push_back(c);
            }
	 }
      }

      void createMainWindow() {
         namedWindow(MAIN_WIN,CV_WINDOW_NORMAL);
         cvSetWindowProperty(MAIN_WIN.c_str(), CV_WND_PROP_FULLSCREEN, main_window_size);
         moveWindow(MAIN_WIN,0,0);
      }

      Mat convert(Mat prev, Mat curr) {
         my::VideoAlgorithm* va=dynamic_cast<my::VideoAlgorithm*>(alg);
         if (va==NULL)
            return alg->convert(curr);
         else
            return va->convert(prev,curr);
      }
   public:
      State(my::Algorithm* a) : alg(a) {
         MAIN_WIN="Main";
         MAIN_WINDOW_WIDTH=1300;
         MAIN_WINDOW_HEIGHT=700;
         main_window_size=CV_WINDOW_FULLSCREEN;
         selectedCapInd=0;
         selectedParamInd=0;
         applyAlg=my::APPLY_AFTER_RESIZE;
         info_page=INFO_PARAMS;
         useYUVspace=false;
         createMainWindow();
         loadCaptures(getFileList("../test_video_temp1/"));
         writeVideo=false;
      }


      ~State() {
         if (writeVideo)
           videoWriter.release();
      }
      my::AlgorithmApplication applyAlgorithm() {
         return applyAlg;
      }
 
      void setApplyAlgorithm(my::AlgorithmApplication value) {
         applyAlg=value;
      }

      void setVideoOutput(char* file) {
         writeVideo=true;
         videoWriter=VideoWriter(file,VideoWriter::fourcc('H','2','6','4'),7.0,Size(getLargeImageWidth(),getLargeImageHeight()),true);
      }

      int getSelectedCapInd() {
         return selectedCapInd;
      }

      int setSelectedParam(int index) {
         if (index<=0 || index > 9)
            throw "Parameter index out of range.";
         if (index > alg->getParams().size())
            index = alg->getParams().size();
         selectedParamInd=index-1;
      }

      void increaseSelectedParam(int steps) {
         alg->getParams()[selectedParamInd]->increase(steps);
      }

      int getCaptureCount() {
         return captures.size();
      }

      void nextFrame() {
         
         prev=curr;
         curr.clear();
         for (int i=0; i<captures.size(); i++) {
            Mat tmp;
            if (!captures[i].read(tmp)) {
               cout << "End of video file." << endl;
               captures[i].release();
               captures[i].open(files[i]);
               captures[i] >> tmp;
            }
            curr.push_back(tmp);
         }
      }

      void skip(int frames, int ind) {
         for ( ; frames > 0; frames--) {
            if (!captures[ind].grab()) {
               cout << "End of video file." << endl;
               captures[ind].release();
               captures[ind].open(files[ind]);
               captures[ind].grab();
            }
         }
            
         Mat tmp;
         if (!captures[ind].read(tmp)) {
            cout << "End of video file." << endl;
            captures[ind].release();
            captures[ind].open(files[ind]);
            captures[ind] >> tmp;
         }
         prev[ind]=tmp;
         if (!captures[ind].read(tmp)) {
            cout << "End of video file." << endl;
            captures[ind].release();
            captures[ind].open(files[ind]);
            captures[ind] >> tmp;
         }
         curr[ind]=tmp;
      }

      void skipCurrent(int frames) {
         skip(frames,selectedCapInd);
      }

      void skipAll(int frames) {
         for (int i=0 ; i<captures.size(); i++) {
            skip(frames,i);
         }
      }

      Mat getImage(int index) {
         Mat p=prev[index];
         Mat c=curr[index];
         Mat img;
         if (applyAlg==my::APPLY_BEFORE_RESIZE) 
            img=convert(p,c);
         if (applyAlg==my::APPLY_AFTER_RESIZE) {
            img=convert(p,c);
         }
         return img;
      }

      Mat getSelectedImage() {
         return getImage(selectedCapInd);
      }

      Mat getImage(int index, Size size) {
         Mat p=prev[index];
         Mat c=curr[index];
         Mat img;
         c.copyTo(img);
         if (useYUVspace) {
            Mat p2,c2;
            cvtColor(p,p2,CV_BGR2YUV);
            cvtColor(c,c2,CV_BGR2YUV);
            p=p2;
            c=c2;
         }
         if (applyAlg==my::APPLY_BEFORE_RESIZE) 
            img=convert(p,c);
         resize(p,p,size);
         resize(c,c,size);
         resize(img,img,size);
         if (applyAlg==my::APPLY_AFTER_RESIZE) {
            img=convert(p,c);
         }
         return img;
      }

      Mat getSelectedImage(Size size) {
         Mat img=getImage(selectedCapInd,size);
         // Write to video file
         if (writeVideo) {
            videoWriter << img;
         }
         return img;
      }

      string getMainWindowName() {
         return MAIN_WIN;
      }

      void setMainWindowSize(double size) { 
         if (size!=main_window_size) {
            main_window_size=size;
            cvSetWindowProperty(MAIN_WIN.c_str(), CV_WND_PROP_FULLSCREEN, main_window_size);
         }
      }

      double getMainWindowSize() { 
         return main_window_size;
      }

      int getRows() {
         return (int) floor(sqrt(captures.size()));
      }

      int getCols() {
         return captures.size()/getRows();
      }

      int getMainWindowWidth() {
         return MAIN_WINDOW_WIDTH;
      }

      int getMainWindowHeight() {
         return MAIN_WINDOW_HEIGHT;
      }

      int getSmallImageWidth(int index) {
         return getMainWindowWidth()/2/getCols();
      }

      int getSmallImageHeight(int index) {
         return getMainWindowHeight()/getRows();
      }

      int getSmallImageXPos(int index) {
         return getMainWindowWidth()/2+(index%getCols())*getSmallImageWidth(-1);
      }

      int getSmallImageYPos(int index) {
         return 0+(index/getCols())*getSmallImageHeight(-1);
      }

      int getLargeImageWidth() {
         return getMainWindowWidth()/2;
      }

      int getLargeImageHeight() {
         return getMainWindowHeight()/2;
      }

      int getLargeImageXPos() {
         return 0;
      }

      int getLargeImageYPos() {
         return 0;
      }	

      void selectLeftImg() {
         if (selectedCapInd%getCols() > 0)
            selectedCapInd--;
      }

      void selectRightImg() {
         if (selectedCapInd%getCols() < getCols()-1)
            selectedCapInd++;
      }

      void selectUpImg() {
         if (selectedCapInd/getRows() > 0)
            selectedCapInd-=getCols();
      }

      void selectDownImg() {
          if (selectedCapInd/getRows() < getRows())
            selectedCapInd+=getCols();
      }

      void nextInfoPage() {
         info_page=static_cast<InfoType>((info_page+1)%2);
      }

      void changeColorSpace() {
         useYUVspace=!useYUVspace;
      }

      vector<string> getInfo() {
         vector<string> info;
         if (info_page==INFO_HELP) {
            info.push_back("Select images: w,a,s,d");
            info.push_back("Apply algorithm: y");
            info.push_back("Use the YUV color space: x");
            info.push_back("Toggle full screen on/off: f");
            info.push_back("Skip some time in current/all: n/m");
            info.push_back("Help: h");
            info.push_back("Exit: e");
            info.push_back("Select a parameter: 1-9");
            info.push_back("Increase/decrease parameter value: o,l,u,j");
         } else if (info_page==INFO_PARAMS) {
            if (applyAlg==my::ORIGINAL_IMAGE) {
               info.push_back("Showing original images.");
               info.push_back("Hit y key to apply algorithm."); 
            } else {
               if (applyAlg==my::APPLY_BEFORE_RESIZE)
                  info.push_back("Applying algorithm before resize.");
               else
                  info.push_back("Applying algorithm after resize.");
               vector<string> tmp=alg->getInfo();
               for (int i=0; i<tmp.size(); i++)
                  info.push_back(tmp[i]);

	       info.push_back(" ");
               info.push_back("Selected param: "+alg->getParams()[selectedParamInd]->getName());
            }
         }
         return info;
      }
};

Mat drawMainWindow(State s) {
   // Small images
   Scalar red(0,0,255);
   Scalar blue(255,0,0);
   Mat selectedImg=s.getSelectedImage(Size(s.getLargeImageWidth(),s.getLargeImageHeight()));
   Mat mainWin=Mat::zeros(s.getMainWindowHeight(),s.getMainWindowWidth(),selectedImg.type());
   for (int i=0; i<s.getCaptureCount(); i++) {
      Rect r=Rect(s.getSmallImageXPos(i),s.getSmallImageYPos(i),s.getSmallImageWidth(i),s.getSmallImageHeight(i));
      Mat tmp=Mat(mainWin,r);
      Size size=Size(s.getSmallImageWidth(i),s.getSmallImageHeight(i));
      //s.getImage(i,size).copyTo(tmp);
      tmp=Mat::zeros(s.getSmallImageHeight(i),s.getSmallImageWidth(i),selectedImg.type());
      if (s.getSelectedCapInd()!=i)
        rectangle(mainWin,r,blue);
      else
         rectangle(mainWin,r,red);
   }

   // Large image
   Rect r=Rect(s.getLargeImageXPos(),s.getLargeImageYPos(),s.getLargeImageWidth(),s.getLargeImageHeight());
   Mat tmp=Mat(mainWin,r);
   Size size=Size(s.getLargeImageWidth(),s.getLargeImageHeight());
   selectedImg.copyTo(tmp);
   rectangle(mainWin,r,red);

   // Info text
   vector<string> info=s.getInfo();
   Scalar white(240,240,240);
   int fontFace=FONT_HERSHEY_SIMPLEX;
   double fontSize=1.0;
   for (int i=0; i < info.size(); i++) {
      int baseline=0;
      Size textSize=getTextSize(info[i],fontFace,fontSize,8,&baseline);
      putText(mainWin,info[i],Point(0,s.getLargeImageYPos()+s.getLargeImageHeight()*11/10+textSize.height*(i+1)),fontFace,fontSize,white,1,8,false);
   }

   return mainWin;
}

int main(int argc, char** argv )
{
    State state=State(new my::BlendFrames());
    if (argc>=2) state.setVideoOutput(argv[1]);
    imshow(state.getMainWindowName(),drawMainWindow(state));  
    char c=waitKey(500);
    while (c!='e') {
       if (c=='f') {
           if (state.getMainWindowSize()==CV_WINDOW_FULLSCREEN)
              state.setMainWindowSize(CV_WINDOW_NORMAL);
           else
              state.setMainWindowSize(CV_WINDOW_FULLSCREEN);
       } else if (c=='w') 
              state.selectUpImg();
       else if (c=='s') 
              state.selectDownImg();
       else if (c=='a') 
              state.selectLeftImg();
       else if (c=='d') 
              state.selectRightImg();
       else if (c=='y')
              state.setApplyAlgorithm(static_cast<my::AlgorithmApplication>((state.applyAlgorithm()+1)%3));
       else if (c=='x')
              state.changeColorSpace();
       else if (c=='h')
              state.nextInfoPage();
       else if (c>='1' && c <= '9') {
cout << c << "," << (c-'1') << endl;
              state.setSelectedParam(c-'1'+1);
       } else if (c=='o')
              state.increaseSelectedParam(1);
       else if (c=='l')
              state.increaseSelectedParam(-1);
       else if (c=='u')
              state.increaseSelectedParam(5);
       else if (c=='j')
              state.increaseSelectedParam(-5);
       else if (c=='n')
              state.skipCurrent(30*30);       
       else if (c=='m')
              state.skipAll(30*30);

       imshow(state.getMainWindowName(),drawMainWindow(state));
       c=waitKey(3);
       state.nextFrame();
    }
    return 0;
}
