#include <opencv2/opencv.hpp>
#include "opencv2/imgcodecs.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include <iostream>
#include <math.h>
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <dirent.h>
#include <queue>

using namespace std;
using namespace cv;

string mainWinName="main";
vector<Point> points;

void mouseClick(int event, int x, int y, int flags, void* userdata) {
   if (event == EVENT_LBUTTONDOWN) {
      points.push_back(Point(x,y));
   } 
}

void addColor(vector<Vec3b>& bank, Vec3b color) {
   for (int i=0; i < bank.size(); i++)
      if (bank[i] == color)
         return;
   bank.push_back(color);
}

int main(int argc, char** argv )
{
    Mat img=imread(argv[1]);
    Mat t;
    img.copyTo(t);
    normalize(t,img,0,255,NORM_MINMAX);
    imshow(mainWinName,img);
    setMouseCallback(mainWinName,mouseClick,NULL);
    
    char c=waitKey(500);
    while (c!='e') {
       Mat tmp;
       img.copyTo(tmp);
       for (int i=0; i< points.size(); i++) {
          circle(tmp,points[i],2,Vec3b(0,0,255));
       }

       imshow(mainWinName,tmp);
       c=waitKey(500);
    }

    FileStorage fs("colorbank.xml",FileStorage::READ);
    FileNode root=fs.root();
    vector<Vec3b> colors;
    root["colors"] >> colors;
    for (int i=0; i< points.size(); i++) {
       Vec3b c=img.at<Vec3b>(points[i].y,points[i].x);
       addColor(colors,c);
    }
    fs.release();
  
    for (int i=0 ; i < colors.size() ; i++)
       cout << colors[i] << endl;
    fs=FileStorage("colorbank.xml",FileStorage::WRITE);
    root=fs.root();
    fs <<  "colors" << colors;
    fs.release();

    return 0;
}
