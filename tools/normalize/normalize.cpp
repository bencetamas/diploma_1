#include <opencv2/opencv.hpp>
#include "opencv2/imgcodecs.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include <iostream>
#include <math.h>
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <dirent.h>
#include <queue>
#include <fstream>

using namespace cv;
using namespace std;

int main(int argc, char** argv )
{
    Mat img;
    img=imread(argv[1]);
    Mat tmp;
    normalize(img,tmp,0,255,NORM_MINMAX);
    imwrite(argv[1],tmp);
    return 0;
}

