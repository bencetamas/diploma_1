#ifndef __ALGORITHM_BASE_HPP__
#define __ALGORITHM_BASE_HPP__

#include <opencv2/opencv.hpp>
#include <string>
#include <stdio.h>

using namespace std;
using namespace cv;

namespace my {

enum AlgorithmApplication {ORIGINAL_IMAGE, APPLY_BEFORE_RESIZE, APPLY_AFTER_RESIZE};

class ParamBase {
   public:
      virtual string getName()=0;
      virtual string getInfo()=0;
      virtual void increase(int steps=1)=0;
      virtual void decrease(int steps=1)=0;
};

template<class T>
class Param : public ParamBase {
   private:
      string name;
      T value;
      T stepsize;
      T min;
      T max;
   public:
      Param(string name,T value, T stepsize, T min, T max) :
         name(name),
         value(value),
         stepsize(stepsize),
         min(min),
         max(max) {
         if (this->min>this->max)
            throw "Invalid parameters for my::Param.";
         if (this->value<this->min)
            this->value=this->min;
         if (this->value>this->max)
            this->value=this->max;
      }

      string getName() {
         return name;
      }
  
      T getValue() {
         return value;
      }

      string getInfo() {
         ostringstream ss;
         ss << getName() << ":" << getValue();
         return ss.str();
      }

      void increase(int steps) {
         value=value+steps*stepsize;
         if (value<min)
            value=min;
         if (value>max)
            value=max;
      }

      void decrease(int steps) {
         increase(-1*steps);
      }

};

class Algorithm {
   public:
      virtual Mat convert(Mat original)=0;
      virtual vector<ParamBase*> getParams()=0;
      virtual string getName()=0;
   
      vector<string> getInfo() {
         vector<string> info;
         info.push_back(getName());
         info.push_back(" ");
         vector<ParamBase*> params=getParams();
         for (int i=0; i<params.size(); i++) 
            info.push_back(params[i]->getInfo());
         return info;
      }

};

}
#endif
