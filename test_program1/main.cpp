#include <opencv2/opencv.hpp>
#include "opencv2/imgcodecs.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include <iostream>
#include <math.h>
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <dirent.h>
#include <queue>

#include "algorithms_base.hpp"
#include "algorithms/canny.hpp"
#include "algorithms/cannyandhoughline.hpp"
#include "algorithms/reducecolors.hpp"
#include "algorithms/goodfeaturestotrack.hpp"
#include "algorithms/floodfill.hpp"
#include "algorithms/floodfill_tmp.hpp"
#include "algorithms/colorspaces.hpp"
#include "algorithms/bilateralfilter.hpp"
#include "algorithms/colorbank.hpp"
#include "algorithms/compressx.hpp"
#include "algorithms/cannyandthreshold.hpp"
#include "algorithms/edgeorientation.hpp"
#include "algorithms/edgedensity.hpp"
#include "algorithms/kmeans.hpp"
#include "algorithms/detectline.hpp"
#include "algorithms/gabor.hpp"
#include "algorithms/cannyandlineextract.hpp"
#include "algorithms/comparehist.hpp"

using namespace cv;
using namespace std;

class State {
   private:
      string MAIN_WIN;
      int MAIN_WINDOW_WIDTH;
      int MAIN_WINDOW_HEIGHT;
      double main_window_size;
      vector<Mat> images;
      int selectedImgInd;
      int selectedParamInd;
      enum InfoType {INFO_HELP, INFO_PARAMS} info_page;
      bool useYUVspace;
      
      my::Algorithm* alg;
      my::AlgorithmApplication applyAlg;

      vector<string> getFileList(string dir) {
	DIR *dpdf;
	struct dirent *epdf;
 
	dpdf = opendir(dir.c_str());
	if (dpdf != NULL) {
	   vector<string> result;
	   int i=0;
	   while (epdf = readdir(dpdf)) {
	      result.push_back(string(dir)+string(epdf->d_name));
	      i++;  
	   }
	  
	   closedir(dpdf);
	   return result;
	}
	
	return vector<string>();
      }

      void loadImages(vector<string> files) {
         images.clear();
         for (int i=0; i < files.size(); i++) {
	    Mat tmp=imread(files[i],1);
	    if (tmp.data) {
	      images.push_back(tmp);
            }
	 }
      }

      void createMainWindow() {
         namedWindow(MAIN_WIN,CV_WINDOW_NORMAL);
         cvSetWindowProperty(MAIN_WIN.c_str(), CV_WND_PROP_FULLSCREEN, main_window_size);
         moveWindow(MAIN_WIN,0,0);
      }
   public:
      State(my::Algorithm* a) : alg(a) {
         MAIN_WIN="Main";
         MAIN_WINDOW_WIDTH=1300;
         MAIN_WINDOW_HEIGHT=700;
         main_window_size=CV_WINDOW_FULLSCREEN;
         selectedImgInd=0;
         selectedParamInd=0;
         applyAlg=my::APPLY_AFTER_RESIZE;
         info_page=INFO_PARAMS;
         useYUVspace=false;
         createMainWindow();
         loadImages(getFileList("../test_pict1_cropped/"));
      }

      my::AlgorithmApplication applyAlgorithm() {
         return applyAlg;
      }
 
      void setApplyAlgorithm(my::AlgorithmApplication value) {
         applyAlg=value;
      }

      int getSelectedImgInd() {
         return selectedImgInd;
      }

      int setSelectedParam(int index) {
         if (index<=0 || index > 9)
            throw "Parameter index out of range.";
         if (index > alg->getParams().size())
            index = alg->getParams().size();
         selectedParamInd=index-1;
      }

      void increaseSelectedParam(int steps) {
         alg->getParams()[selectedParamInd]->increase(steps);
      }

      int getImageCount() {
         return images.size();
      }

      Mat getImage(int index) {
         Mat img=images[index];
         if (applyAlg==my::APPLY_BEFORE_RESIZE) 
            img=alg->convert(img);
         if (applyAlg==my::APPLY_AFTER_RESIZE) {
            img=alg->convert(img);
         }
         return img;
      }

      Mat getSelectedImage() {
         return getImage(selectedImgInd);
      }

      Mat getImage(int index, Size size) {
         Mat img=images[index];
         if (useYUVspace) {
            Mat img2;
            cvtColor(img,img2,CV_BGR2YUV);
            img=img2;
         }
         if (applyAlg==my::APPLY_BEFORE_RESIZE) 
            img=alg->convert(img);
         resize(img,img,size);
         if (applyAlg==my::APPLY_AFTER_RESIZE) {
            img=alg->convert(img);
         }
         return img;
      }

      Mat getSelectedImage(Size size) {
         return getImage(selectedImgInd,size);
      }

      string getMainWindowName() {
         return MAIN_WIN;
      }

      void setMainWindowSize(double size) { 
         if (size!=main_window_size) {
            main_window_size=size;
            cvSetWindowProperty(MAIN_WIN.c_str(), CV_WND_PROP_FULLSCREEN, main_window_size);
         }
      }

      double getMainWindowSize() { 
         return main_window_size;
      }

      int getRows() {
         return (int) floor(sqrt(images.size()));
      }

      int getCols() {
         return images.size()/getRows();
      }

      int getMainWindowWidth() {
         return MAIN_WINDOW_WIDTH;
      }

      int getMainWindowHeight() {
         return MAIN_WINDOW_HEIGHT;
      }

      int getSmallImageWidth(int index) {
         return getMainWindowWidth()/2/getCols();
      }

      int getSmallImageHeight(int index) {
         return getMainWindowHeight()/getRows();
      }

      int getSmallImageXPos(int index) {
         return getMainWindowWidth()/2+(index%getCols())*getSmallImageWidth(-1);
      }

      int getSmallImageYPos(int index) {
         return 0+(index/getCols())*getSmallImageHeight(-1);
      }

      int getLargeImageWidth() {
         return getMainWindowWidth()/2;
      }

      int getLargeImageHeight() {
         return getMainWindowHeight()/2;
      }

      int getLargeImageXPos() {
         return 0;
      }

      int getLargeImageYPos() {
         return 0;
      }	

      void selectLeftImg() {
         if (selectedImgInd%getCols() > 0)
            selectedImgInd--;
      }

      void selectRightImg() {
         if (selectedImgInd%getCols() < getCols()-1)
            selectedImgInd++;
      }

      void selectUpImg() {
         if (selectedImgInd/getRows() > 0)
            selectedImgInd-=getCols();
      }

      void selectDownImg() {
          if (selectedImgInd/getRows() < getRows())
            selectedImgInd+=getCols();
      }

      void nextInfoPage() {
         info_page=static_cast<InfoType>((info_page+1)%2);
      }

      void changeColorSpace() {
         useYUVspace=!useYUVspace;
      }

      vector<string> getInfo() {
         vector<string> info;
         if (info_page==INFO_HELP) {
            info.push_back("Select images: w,a,s,d");
            info.push_back("Apply algorithm: y");
            info.push_back("Use the YUV color space: x");
            info.push_back("Toggle full screen on/off: f");
            info.push_back("Help: h");
            info.push_back("Exit: e");
            info.push_back("Select a parameter: 1-9");
            info.push_back("Increase/decrease parameter value: o,l,u,j");
         } else if (info_page==INFO_PARAMS) {
            if (applyAlg==my::ORIGINAL_IMAGE) {
               info.push_back("Showing original images.");
               info.push_back("Hit y key to apply algorithm."); 
            } else {
               if (applyAlg==my::APPLY_BEFORE_RESIZE)
                  info.push_back("Applying algorithm before resize.");
               else
                  info.push_back("Applying algorithm after resize.");
               vector<string> tmp=alg->getInfo();
               for (int i=0; i<tmp.size(); i++)
                  info.push_back(tmp[i]);

	       info.push_back(" ");
               info.push_back("Selected param: "+alg->getParams()[selectedParamInd]->getName());
            }
         }
         return info;
      }
};

Mat drawMainWindow(State s) {
   // Small images
   Scalar red(0,0,255);
   Scalar blue(255,0,0);
   Mat selectedImg=s.getSelectedImage(Size(s.getLargeImageWidth(),s.getLargeImageHeight()));
   Mat mainWin=Mat::zeros(s.getMainWindowHeight(),s.getMainWindowWidth(),selectedImg.type());
   for (int i=0; i<s.getImageCount(); i++) {
      Rect r=Rect(s.getSmallImageXPos(i),s.getSmallImageYPos(i),s.getSmallImageWidth(i),s.getSmallImageHeight(i));
      Mat tmp=Mat(mainWin,r);
      Size size=Size(s.getSmallImageWidth(i),s.getSmallImageHeight(i));
      //s.getImage(i,size).copyTo(tmp);
      tmp=Mat::zeros(s.getSmallImageHeight(i),s.getSmallImageWidth(i),selectedImg.type());
      if (s.getSelectedImgInd()!=i)
        rectangle(mainWin,r,blue);
      else
         rectangle(mainWin,r,red);
   }
   // Large image
   Rect r=Rect(s.getLargeImageXPos(),s.getLargeImageYPos(),s.getLargeImageWidth(),s.getLargeImageHeight());
   Mat tmp=Mat(mainWin,r);
   Size size=Size(s.getLargeImageWidth(),s.getLargeImageHeight());
   selectedImg.copyTo(tmp);
   rectangle(mainWin,r,red);
   // Info text
   vector<string> info=s.getInfo();
   Scalar white(240,240,240);
   int fontFace=FONT_HERSHEY_SIMPLEX;
   double fontSize=1.0;
   for (int i=0; i < info.size(); i++) {
      int baseline=0;
      Size textSize=getTextSize(info[i],fontFace,fontSize,8,&baseline);
      putText(mainWin,info[i],Point(0,s.getLargeImageYPos()+s.getLargeImageHeight()*11/10+textSize.height*(i+1)),fontFace,fontSize,white,1,8,false);
   }
   return mainWin;
}

int main(int argc, char** argv )
{
    State state=State(new my::CompareHist());
    imshow(state.getMainWindowName(),drawMainWindow(state));
    
    char c=waitKey(500);
    while (c!='e') {
       if (c=='f') {
           if (state.getMainWindowSize()==CV_WINDOW_FULLSCREEN)
              state.setMainWindowSize(CV_WINDOW_NORMAL);
           else
              state.setMainWindowSize(CV_WINDOW_FULLSCREEN);
       } else if (c=='w') 
              state.selectUpImg();
       else if (c=='s') 
              state.selectDownImg();
       else if (c=='a') 
              state.selectLeftImg();
       else if (c=='d') 
              state.selectRightImg();
       else if (c=='y')
              state.setApplyAlgorithm(static_cast<my::AlgorithmApplication>((state.applyAlgorithm()+1)%3));
       else if (c=='x')
              state.changeColorSpace();
       else if (c=='h')
              state.nextInfoPage();
       else if (c>='1' && c <= '9') {
cout << c << "," << (c-'1') << endl;
              state.setSelectedParam(c-'1'+1);
       } else if (c=='o')
              state.increaseSelectedParam(1);
       else if (c=='l')
              state.increaseSelectedParam(-1);
       else if (c=='u')
              state.increaseSelectedParam(5);
       else if (c=='j')
              state.increaseSelectedParam(-5);

       imshow(state.getMainWindowName(),drawMainWindow(state));
       c=waitKey(500);
    }
    return 0;
}
