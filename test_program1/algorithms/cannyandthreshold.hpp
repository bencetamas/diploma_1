#ifndef __CANNYANDTHRESHOLD_HPP__
#define __CANNYANDTHRESHOLD_HPP__

#include <opencv2/opencv.hpp>
#include "opencv2/imgcodecs.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include <iostream>
#include <math.h>
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <dirent.h>
#include <queue>

#include "../algorithms_base.hpp"

using namespace std;
using namespace cv;

namespace my {

class CannyAndThreshold : public Algorithm {
      Param<int> threshLow;
      Param<int> threshHigh;
      Param<int> filter;
      Param<int> cannyMinTreshold;
      Param<int> cannyMaxTreshold;
      Param<int> cannySize;
      Param<int> morphSize;
   public:
      CannyAndThreshold() :
         threshLow("threshold low value",6,1,0,255),
         threshHigh("threshold high value",68,1,0,255),
         filter("filter size",9,1,0,64),
         cannyMinTreshold("canny min tresh",48,1,0,255),
         cannyMaxTreshold("canny max tresh",160,1,0,255),
         cannySize("canny size",3,2,1,255),
         morphSize("morpholgy size",9,1,0,255) {
      }

      Mat convert(Mat original) {
         Mat result;
         normalize(original,original,0,255,NORM_MINMAX);
         original.copyTo(result);
         Mat tmp;
         Canny(result,tmp,cannyMinTreshold.getValue(),cannyMaxTreshold.getValue(),cannySize.getValue());
         boxFilter(tmp,result,-1,Size(filter.getValue()*2+1,filter.getValue()*2+1));
//distanceTransform(result,tmp,CV_DIST_L1,3);
//normalize(tmp,result,0,255.0,NORM_MINMAX);

         threshold(result,result,threshLow.getValue(),255,THRESH_TOZERO);
         threshold(result,result,threshHigh.getValue(),255,THRESH_TOZERO_INV);
//Mat element = getStructuringElement( 0, Size( 2*morphSize.getValue() + 1, 2*morphSize.getValue()+1 ), Point( -1,-1 ) );
//morphologyEx(result,result,MORPH_OPEN,element);
//erode(result,result,element);
normalize(result,result,0,1.0,NORM_MINMAX);
         cvtColor(result,result,CV_GRAY2BGR);
return result;
         return result*0.8+original*0.2;
      }

      vector<ParamBase*> getParams() {
         vector<ParamBase*> params;
         params.push_back(&threshLow);
         params.push_back(&threshHigh);
         params.push_back(&filter);
         params.push_back(&cannyMinTreshold);
         params.push_back(&cannyMaxTreshold);
         params.push_back(&cannySize);
         params.push_back(&morphSize);
         return params;
      }

      string getName() {
         return "compressing in x direction";
      }
};

}
#endif
