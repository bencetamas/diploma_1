class GrabCut : public Algorithm {
      Param<int> x1;
      Param<int> x2;
      Param<int> y1;
      Param<int> y2;
      Param<int> iterations;
   public:
      GrabCut() :
         x1("x1",25,1,0,100),
         x2("x2",25,1,0,100),
         y1("y1",25,1,0,100),
         y2("y2",25,1,0,100),
         iterations("iterations",1,1,0,200) {
      }

      Mat convert(Mat original) {
Mat result;
original.copyTo(result);
for (int i=0; i<6; i++) {
            Mat tmp;
            bilateralFilter(result,tmp,9,9,7);
            tmp.copyTo(result);
            //adaptiveBilateralFilter(original,result,filterSize.getValue(),sigmaColor.getValue(),sigmaSpace.getValue());
         }
result.copyTo(original);


         Mat tmp1,tmp2;
         Mat mask;
         Rect r=Rect(x1.getValue()*original.cols/100,y1.getValue()*original.rows/100,
                 x2.getValue()*original.cols/100,y2.getValue()*original.rows/100);
         Mat tmp;
         original.copyTo(tmp);
         grabCut(tmp,mask,r,tmp1,tmp2,iterations.getValue(),
                 GC_INIT_WITH_RECT);
         rectangle(tmp,r,Scalar(0,0,255));
cvtColor(tmp,tmp,CV_BGR2GRAY);
/*
Mat tmp22 = cv::Mat::zeros( tmp.rows, tmp.cols, CV_32FC1 ); 
    for( int r=0; r<tmp.rows; ++r)
        for( int c=0; c<tmp.cols; ++c)
            tmp22.at<float>(r,c) = (float) tmp.at<u_int8_t>( r,c ); 
Mat mask2 = cv::Mat::zeros( mask.rows, mask.cols, CV_32FC1 ); 
    for( int r=0; r<mask.rows; ++r)
        for( int c=0; c<mask.cols; ++c)
            mask2.at<float>(r,c) = (float) mask.at<u_int8_t>( r,c ); */
Mat t1,t2;
threshold(mask,t1,GC_FGD-1,255,THRESH_BINARY);
threshold(mask,t2,GC_FGD+1,255,THRESH_BINARY_INV);
t2=255-t2;
t1.convertTo(t1,CV_64FC1);
t2.convertTo(t2,CV_64FC1);
normalize(t1,t1,1,0,NORM_MINMAX);
normalize(t2,t2,1,0,NORM_MINMAX);

tmp.convertTo(tmp,CV_64FC1);
normalize(tmp,tmp,1,0,NORM_MINMAX);
mask.convertTo(mask,CV_64FC1);
normalize(mask,mask,1,0,NORM_MINMAX);
Mat w;

Mat mask2 = cv::Mat::zeros( mask.rows, mask.cols, CV_64FC1 ); 
    for( int r=0; r<mask.rows; ++r)
        for( int c=0; c<mask.cols; ++c)
            mask2.at<float>(r,c) = (float) tmp.at<float>( r,c )*t1.at<float>( r,c )*t2.at<float>( r,c ); 
normalize(mask2,mask2,1,0,NORM_MINMAX);
//addWeighted(mask*50,0.5,tmp,0.5,0,w);

         return mask2;
      }

      vector<ParamBase*> getParams() {
         vector<ParamBase*> params;
         params.push_back(&x1);
         params.push_back(&x2);
         params.push_back(&y1);
         params.push_back(&y2);
         params.push_back(&iterations);
         return params;
      }

      string getName() {
         return "graph cut";
      }
};
