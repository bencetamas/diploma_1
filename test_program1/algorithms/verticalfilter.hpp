class VerticalFilter1 : public Algorithm {
      Param<int> filterSize;
   public:
      VerticalFilter1() :
         filterSize("Blur filter size",3,2,1,15) {
      }

      Mat convert(Mat original) {
         Mat result; 
         medianBlur(original, result, filterSize.getValue());
         vector<Mat> chs;
         split(result,chs);
         for (int i=0; i < chs.size(); i++) {
            Mat kernelX=Mat::ones(1,1,chs[i].type());
            //Mat kernelY=(Mat_<int>(28,1) << -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1, 1,1,1,1,1,1,1,1,1,1,1,1,1,1);
            Mat kernelY=(Mat_<int>(2,1) << -1, 1);

            sepFilter2D(chs[i],chs[i],-1,kernelX,kernelY);
         }
         merge(chs,result);
         return result;
      }

      vector<ParamBase*> getParams() {
         vector<ParamBase*> params;
         params.push_back(&filterSize);
         return params;
      }

      string getName() {
         return "simple vertical edge filter";
      }
};

