#ifndef __COMPAREHIST_HPP__
#define __COMPAREHIST_HPP__

#include <opencv2/opencv.hpp>
#include "opencv2/imgcodecs.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include <iostream>
#include <math.h>
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <dirent.h>
#include <queue>

#include "../algorithms_base.hpp"

using namespace std;
using namespace cv;

namespace my {

class CompareHist : public Algorithm {
      Param<double> x1;
      Param<double> y1;
      Param<double> x2;
      Param<double> y2;
      Param<int> xsize;
      Param<int> ysize;

      int binNum;
   public:
      CompareHist() :
         x1("x1",0.0,1,0.0,100),
         y1("y1",80,1,0.0,100),
         x2("x2",50,1,0.0,100),
         y2("y2",50,1,0.0,100),
         xsize("xsize",200,1,1,300),
         ysize("ysize",10,1,1,200) {
         binNum=20;
      }

      void getHistogram(Mat img, int x, int y,int dx, int dy, MatND hist[3]) {
         Rect roiR=Rect(Point(x,y),Point(x+dx,y+dy));
         Mat roi=img(roiR);

         int histBins[]={binNum};
         float histRanges[]={0,255.0};
         const float* ranges={histRanges};
         int channels[]={1};

         for (int i=0; i < 3 ;i++) {
            int channels[]={i};
            calcHist(&roi,1,channels,Mat(),hist[i],1,histBins,&ranges,true,false);
         }

         rectangle(img,roiR,Scalar(0,255,0));
      }

      void drawHist(MatND hist[3], Mat& img, int posPercent, double scale=1.0) {
          for (int i=0; i<3; i++) {
            double maxVal=0;
            minMaxLoc(hist[i], 0, &maxVal, 0, 0);
            Scalar color=Scalar(0,0,0);
            color[i]=255;
            for( int p = 0; p < binNum; p++ )
            {
               float binVal = hist[i].at<float>(p);
               int intensity = cvRound(binVal*img.rows/4/maxVal)*scale;
               int bottom=cvRound(posPercent/100.0*img.rows);
               int x=img.cols*p/binNum;
               rectangle( img, Point(x*scale, bottom-intensity),
                           Point( (x+cvRound(img.cols/binNum))*scale - 1, bottom),
                           //Scalar::all(intensity),
                           color,
                           CV_FILLED );
            }
         }
      }

      Mat convert(Mat original) {
         Mat tmp;
         normalize(original,tmp,0,255,NORM_MINMAX);
         Mat result;
         original.copyTo(result);

         MatND hist1[3];
         getHistogram(result,x1.getValue()*original.cols/100.0,y1.getValue()*original.rows/100.0,xsize.getValue(),ysize.getValue(),hist1);
         MatND hist2[3];
         getHistogram(result,x2.getValue()*original.cols/100.0,y2.getValue()*original.rows/100.0,xsize.getValue(),ysize.getValue(),hist2);

         for (int i=0; i< 3; i++) {
            MatND tmp1,tmp2;
            normalize( hist1[i], tmp1, 0, 1, NORM_MINMAX, -1, Mat() );
            normalize( hist2[i], tmp2, 0, 1, NORM_MINMAX, -1, Mat() );
            double comp1=compareHist(tmp1,tmp2,CV_COMP_CORREL);
            double comp2=compareHist(tmp1,tmp2,CV_COMP_CHISQR);
            double comp3=compareHist(tmp1,tmp2,CV_COMP_INTERSECT);
            double comp4=compareHist(tmp1,tmp2,CV_COMP_BHATTACHARYYA);
            std::ostringstream s; 
            s << comp1 << ", " << comp2 << ", " << comp3 << ", " << comp4;
            putText(result,string("")+string(s.str()),Point(5,25+i*30),FONT_HERSHEY_SIMPLEX,1.0,Scalar(100,0,255));
         }

         result=result*0.4;
         drawHist(hist1,result,100);
         drawHist(hist2,result,70);

         return result;
      }

      vector<ParamBase*> getParams() {
         vector<ParamBase*> params;
         params.push_back(&x1);
         params.push_back(&y1);
         params.push_back(&x2);
         params.push_back(&y2);
         params.push_back(&xsize);
         params.push_back(&ysize);
         return params;
      }

      string getName() {
         return "compare histograms";
      }
};

} 

#endif

