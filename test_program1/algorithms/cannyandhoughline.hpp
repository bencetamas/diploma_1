#ifndef __CANNYANDHOUGHLINES_HPP__
#define __CANNYANDHOUGHLINES_HPP__

#include <opencv2/opencv.hpp>
#include "opencv2/imgcodecs.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include <iostream>
#include <math.h>
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <dirent.h>
#include <queue>

#include "../algorithms_base.hpp"

using namespace std;
using namespace cv;

namespace my {

class CannyAndHoughLines : public Algorithm {
      Param<int> minTreshold;
      Param<int> maxTreshold;
      Param<int> rho;
      Param<double> theta;
      Param<int> houghTreshold;
      Param<double> houghMin;
      Param<double> houghMax;
      Param<int> filterSize;
   public:
      CannyAndHoughLines() :
         minTreshold("Canny min. treshold",100,1,0,255),
         maxTreshold("Canny max. treshold",200,1,0,255),
         rho("Hough rho val.",1,1,1,25),
         theta("Hough theta val.",3.14/180,3.14/720,3.14/720,3.14),
         houghTreshold("Hough treshold param",1,1,0,100),
         houghMin("Hough min line length",10,1,1,255),
         houghMax("Hough max line gap",2,1,1,255),
         filterSize("Blur filter size",3,2,1,15) {
      }

      Mat convert(Mat original) {

         Mat result=original;
	 //GaussianBlur(original, result, Size(filterSize.getValue(),filterSize.getValue()),0,0);
         //blur(original, result, Size(filterSize.getValue(),filterSize.getValue()),Point(-1,-1));
         //medianBlur(original, result, filterSize.getValue());
	Mat result2;
         Canny(result,result2,minTreshold.getValue(),maxTreshold.getValue(),3);
         vector<Vec4i> lines;
         HoughLinesP(result2,lines,rho.getValue(),theta.getValue(),houghTreshold.getValue(),houghMin.getValue(),houghMax.getValue());
	 cvtColor(result2,result2,CV_GRAY2BGR);
         result2=result2*0.8;
         for( size_t i = 0; i < lines.size(); i++ ){
            line( result2, Point(lines[i][0], lines[i][1]),
            Point(lines[i][2], lines[i][3]), Scalar(0,0,255), 1, 8 );
         }

         return result2;
      }

      vector<ParamBase*> getParams() {
         vector<ParamBase*> params;
         params.push_back(&minTreshold);
         params.push_back(&maxTreshold);
         params.push_back(&rho);
         params.push_back(&theta);
         params.push_back(&houghTreshold);
         params.push_back(&houghMin);
         params.push_back(&houghMax);
         params.push_back(&filterSize);
         return params;
      }

      string getName() {
         return "Canny edge detection and Hough line algorithm";
      }


};

}

#endif
