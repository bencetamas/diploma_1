#ifndef __KMEANS_HPP__
#define __KMEANS_HPP__

#include <opencv2/opencv.hpp>
#include "opencv2/imgcodecs.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include <iostream>
#include <math.h>
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <dirent.h>
#include <queue>

#include "../algorithms_base.hpp"

using namespace std;
using namespace cv;

namespace my {

class KMeans1 : public Algorithm {
      Param<int> k;
   public:
      KMeans1() :
         k("k",6,1,0,100) {
      }

      Mat convert(Mat original) {
         /*Mat features( original.cols*original.rows, 3, CV_32FC1);
         for (int r=0; r<original.rows; r++)
            for( int c=0; c < original.cols; c++)
               features.at<float>(r*original.cols+c,0)=original.at<int>

*/
Mat src = original;
Mat element = getStructuringElement( MORPH_RECT, Size( 11, 11 ));
Mat max;
dilate(src,max,element);
Mat min;
erode(src,min,element);
  Mat samples(src.rows * src.cols, 9, CV_32F);
  for( int y = 0; y < src.rows; y++ )
    for( int x = 0; x < src.cols; x++ ) {
      for( int z = 0; z < 3; z++)
        samples.at<float>(y + x*src.rows, z) = src.at<Vec3b>(y,x)[z];
      for( int z = 0; z < 3; z++)
        samples.at<float>(y + x*src.rows, 3+z) = max.at<Vec3b>(y,x)[z];
      for( int z = 0; z < 3; z++)
        samples.at<float>(y + x*src.rows, 6+z) = min.at<Vec3b>(y,x)[z];
}


  int clusterCount = k.getValue();
  Mat labels;
  int attempts = 5;
  Mat centers;
  kmeans(samples, clusterCount, labels, TermCriteria(CV_TERMCRIT_ITER|CV_TERMCRIT_EPS, 10000, 0.0001), attempts, KMEANS_PP_CENTERS, centers );


  Mat new_image( src.size(), src.type() );
  for( int y = 0; y < src.rows; y++ )
    for( int x = 0; x < src.cols; x++ )
    { 
      int cluster_idx = labels.at<int>(y + x*src.rows,0);
      new_image.at<Vec3b>(y,x)[0] = centers.at<float>(cluster_idx, 0);
      new_image.at<Vec3b>(y,x)[1] = centers.at<float>(cluster_idx, 1);
      new_image.at<Vec3b>(y,x)[2] = centers.at<float>(cluster_idx, 2);
    }
return new_image;
      }

      vector<ParamBase*> getParams() {
         vector<ParamBase*> params;
         params.push_back(&k);
         return params;
      }

      string getName() {
         return "kmeans";
      }
};

class KMeansLawsMask : public Algorithm {
      Param<int> k;
      Param<int> w1;
      Param<int> w2;
      Param<int> binCount;

      vector<int> vecFromArray(int array[],int size) {
         vector<int> result;
         for (int i=0; i<size; i++)
            result.push_back(array[i]);
         return result;
      }
   public:
      KMeansLawsMask() :
         k("k - kmeans",6,1,0,100),
         w1("ill. window size",15,2,1,80),
         w2("sum. window size",7,2,1,256),
         binCount("bin count",16,8,1,256) {
      }

      Mat convert(Mat original) {
        int l5_[5]={ 1, 4, 6, 4, 1};
        int e5_[5]={ 1, -2, 0, 2, -1};
        int s5_[5]={ -1, 0, 2, 0, -1};
        int r5_[5]={ 1, -4, 6, -4, 1};
        vector<int> l5=vecFromArray(l5_,5);
        vector<int> e5=vecFromArray(e5_,5);
        vector<int> s5=vecFromArray(s5_,5);
        vector<int> r5=vecFromArray(r5_,5);

         Mat src;
         cvtColor(original,src,CV_BGR2GRAY);
 
         Mat intensity;
         boxFilter(src,intensity,-1,Size(w1.getValue(),w1.getValue()));
         src=src-intensity;
//normalize(src,src,0,255,NORM_MINMAX);
//return src;
         Mat laws[9];
         Mat tmp1,tmp2;
         sepFilter2D(src,tmp1,-1,l5,e5);
         sepFilter2D(src,tmp2,-1,e5,l5);
         laws[0]=tmp1*0.5+tmp2*0.5;
         sepFilter2D(src,tmp1,-1,l5,s5);
         sepFilter2D(src,tmp2,-1,s5,l5);
         laws[1]=tmp1*0.5+tmp2*0.5;
         sepFilter2D(src,tmp1,-1,l5,r5);
         sepFilter2D(src,tmp2,-1,r5,l5);
         laws[2]=tmp1*0.5+tmp2*0.5;
         sepFilter2D(src,laws[3],-1,e5,e5);
         sepFilter2D(src,tmp1,-1,s5,e5);
         sepFilter2D(src,tmp2,-1,e5,s5);
         laws[4]=tmp1*0.5+tmp2*0.5;
         sepFilter2D(src,tmp1,-1,r5,e5);
         sepFilter2D(src,tmp2,-1,e5,r5);
         laws[5]=tmp1*0.5+tmp2*0.5;
         sepFilter2D(src,laws[6],-1,s5,s5);
         sepFilter2D(src,tmp1,-1,s5,r5);
         sepFilter2D(src,tmp2,-1,r5,s5);
         laws[7]=tmp1*0.5+tmp2*0.5;
         sepFilter2D(src,laws[8],-1,r5,r5);
   
         for (int i=0; i<9; i++) {
            boxFilter(laws[i],laws[i],-1,Size(w2.getValue(),w2.getValue()));
            normalize(laws[i],laws[i],0,255,NORM_MINMAX);
         }
cout << "Phase 1 done" << endl;
         Mat samples(src.rows * src.cols, 9*binCount.getValue(), CV_32F);
         for( int y = 0; y < src.rows-15; y++ )
            for( int x = 0; x < src.cols-15; x++ ) {
               Mat hist[9];
               for (int f=0; f < 9 ; f++) {
                  Rect ROI(Point(x,y),Point(x+15,y+15));
                  Mat window=Mat(laws[f],ROI);
                  float range[] = { 0, 256 } ;
                  const float* histRange = { range };
                  int histSize=binCount.getValue();
                  calcHist(&window,1,0,Mat(),hist[f],1,&histSize,&histRange,true,false);
                  hist[f]=hist[f].t();
                  hist[f].copyTo(samples.row(y + x*src.rows).colRange(f*binCount.getValue(),(f+1)*binCount.getValue()));
               }
           }
cout << "Phase 2 done" << endl;
cout << samples.size() << endl;
         /*Mat samples(src.rows * src.cols, 9, CV_32F);
         for( int y = 0; y < src.rows; y++ )
            for( int x = 0; x < src.cols; x++ ) {
               for (int f=0; f < 9; f++) 
                  samples.at<float>(y + x*src.rows,f)=laws[f].at<int>(y,x);
         }*/

         int clusterCount = k.getValue();
         Mat labels;
         int attempts = 1;
         Mat centers;
         kmeans(samples, clusterCount, labels, TermCriteria(CV_TERMCRIT_ITER|CV_TERMCRIT_EPS,  2, 0.0001), attempts, KMEANS_PP_CENTERS, centers );
cout << "Phase 3 done" << endl;
         Mat new_image( src.size(), original.type() );
         vector<Vec3b> labelColors;
         RNG rng;
         for (int i=0 ; i < clusterCount; i++) {
            labelColors.push_back(Vec3b(rng(),rng(),rng()));
         }
         for( int y = 0; y < src.rows; y++ )
            for( int x = 0; x < src.cols; x++ )
               { 
                  int cluster_idx = labels.at<int>(y + x*src.rows);
                  new_image.at<Vec3b>(y,x)=labelColors[cluster_idx];
              }
         Mat result;
         normalize(new_image,result,0,255,NORM_MINMAX);

         /*for( int y = 0; y < src.rows; y++ )
           for( int x = 0; x < src.cols; x++ )
             cout << ((int) result.at<Vec3b>(y,x)[0]) << endl;*/
         //cvtColor(result,tmp1,CV_GRAY2BGR);
         return result;
      }

      vector<ParamBase*> getParams() {
         vector<ParamBase*> params;
         params.push_back(&k);
         params.push_back(&w1);
         params.push_back(&w2);
         params.push_back(&binCount);
         return params;
      }

      string getName() {
         return "kmeans";
      }
};

class KMeansWithHistograms : public Algorithm {
      Param<int> k;
      Param<int> wsize;
      Param<int> binCount;
   public:
      KMeansWithHistograms() :
         k("k - kmeans",6,1,0,100),
         wsize("window size",1,2,1,35),
         binCount("bin count",32,1,1,256) {
      }

      Mat convert(Mat original) {

Mat src = original;
resize(src,src,src.size()/4,INTER_LINEAR);
Mat samples(src.rows * src.cols, 3*binCount.getValue(), CV_32F);
  for( int y = 0; y < src.rows; y++ ) {
    for( int x = 0; x < src.cols; x++ ) {
      int x1=x-wsize.getValue();
      int x2=x+wsize.getValue();
      int y1=y-wsize.getValue();
      int y2=y+wsize.getValue();
      if (x1<0) {
         x1=0;
         x2=2*wsize.getValue();
      } else if (x2>=src.cols) {
         x1=src.cols-2*wsize.getValue()-1;
         x2=src.cols-1;
      } 
      if (y1<0) {
         y1=0;
         y2=2*wsize.getValue();
      } else if (y2>=src.rows) {
         y1=src.rows-2*wsize.getValue()-1;
         y2=src.rows-1;
      } 
      vector<Mat> src_;
      split(src,src_);
      for( int z = 0; z < 3; z++) {
        Rect ROI(Point(x1,y1),Point(x2,y2));
        Mat window=Mat(src_[z],ROI);
        Mat hist;
        float range[] = { 0, 256 } ;
        const float* histRange = { range };
        int histSize=binCount.getValue();
        calcHist(&window,1,0,Mat(),hist,1,&histSize,&histRange,true,false);
        hist=hist.t();
        hist.copyTo(samples.row(y + x*src.rows).colRange(z*binCount.getValue(),(z+1)*binCount.getValue()));
      }
}
}

  int clusterCount = k.getValue();
  Mat labels;
  int attempts = 5;
  Mat centers;
  kmeans(samples, clusterCount, labels, TermCriteria(CV_TERMCRIT_ITER|CV_TERMCRIT_EPS, 10000, 0.0001), attempts, KMEANS_PP_CENTERS, centers );

  Mat new_image( src.size(), src.type() );
  for( int y = 0; y < src.rows; y++ )
    for( int x = 0; x < src.cols; x++ )
    { 
      int cluster_idx = labels.at<int>(y + x*src.rows,0);
      new_image.at<Vec3b>(y,x)[0] = cluster_idx;
      new_image.at<Vec3b>(y,x)[1] = cluster_idx;
      new_image.at<Vec3b>(y,x)[2] = cluster_idx;
    }
Mat result;
normalize(new_image,result,0,255,NORM_MINMAX);
  for( int y = 0; y < src.rows; y++ )
    for( int x = 0; x < src.cols; x++ )
      cout << ((int) result.at<Vec3b>(y,x)[0]) << endl;
resize(result,result,original.size(),INTER_NEAREST);
return result*0.9+original*0.1;
      }

      vector<ParamBase*> getParams() {
         vector<ParamBase*> params;
         params.push_back(&k);
         params.push_back(&wsize);
         params.push_back(&binCount);
         return params;
      }

      string getName() {
         return "kmeans";
      }
};

}
#endif
