#ifndef __EDGEDENSITY_HPP__
#define __EDGEDENSITY_HPP__

#include <opencv2/opencv.hpp>
#include "opencv2/imgcodecs.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include <iostream>
#include <math.h>
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <dirent.h>
#include <queue>

#include "../algorithms_base.hpp"

using namespace std;
using namespace cv;

namespace my {

class EdgeDensity : public Algorithm {
      Param<int> thres;
      Param<int> filterSize;
      Param<int> method;
      Param<int> method1;
      Param<int> method2;
   public:
      EdgeDensity() :
         thres("threshold",28,1,0,255),
         filterSize("filter size",17,2,1,200),
         method("method id",1,1,1,3),
         method1("method param 1(canny min)",125,1,1,255),
         method2("method param 2(canny max)",146,1,1,255) {
      }

      Mat convert(const Mat original) {
         Mat edge,tmp;
         if (method.getValue()==1) {
            cvtColor(original,tmp,CV_BGR2GRAY); 
            Canny(tmp,edge,method1.getValue(),method2.getValue(),3);
         } else if (method.getValue()==2) {
            Sobel(original,edge,CV_8U,1,0);
         } else if (method.getValue()==3) {
            Sobel(original,edge,CV_8U,0,1);
         }        
         boxFilter(edge,tmp,-1,Size(filterSize.getValue(),filterSize.getValue()));
         edge=tmp;
	 //threshold(edge,tmp,thres.getValue(),255,THRESH_TOZERO_INV);
         threshold(edge,tmp,thres.getValue(),255,THRESH_BINARY_INV);
return tmp;
         Mat masked1;
         Mat masked2;
         original.copyTo(masked1,tmp);
         tmp=Mat::ones(tmp.size(),CV_8U)*255-tmp;
         Mat blurred;
         GaussianBlur(original,blurred,Size(11,11),0);
         blurred.copyTo(masked2,tmp);

         Mat result=masked1+masked2;
         rectangle(result,Rect(Point(0,0),Point(filterSize.getValue(),filterSize.getValue())),Scalar(255,0,0));
Canny(result,tmp,method1.getValue(),method2.getValue(),3);
result=tmp;

/*
result=original*0.4;
vector<Vec4i> lines;
         HoughLinesP(tmp,lines,10,3.14/180*10,2,15,1);
         for( size_t i = 0; i < lines.size(); i++ ){
            line( result, Point(lines[i][0], lines[i][1]),
            Point(lines[i][2], lines[i][3]), Scalar(0,0,255), 1, 8 );
         }*/
         return result;
      }

      vector<ParamBase*> getParams() {
         vector<ParamBase*> params;
         params.push_back(&thres);
         params.push_back(&filterSize);
         params.push_back(&method);
         params.push_back(&method1);
         params.push_back(&method2);
         return params;
      }

      string getName() {
         return "edge density";
      }
};

} 

#endif
