#ifndef __COLORSPACES_HPP__
#define __COLORSPACES_HPP__

#include <opencv2/opencv.hpp>
#include "opencv2/imgcodecs.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include <iostream>
#include <math.h>
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <dirent.h>
#include <queue>

#include "../algorithms_base.hpp"

using namespace std;
using namespace cv;

namespace my {

class ColorSpaces : public Algorithm {
      Param<int> code;
   public:
      ColorSpaces() :
         code("Color space code",0,1,0,10)
          {
      }

      Mat convert(Mat original) {
         normalize(original,original,0,255,NORM_MINMAX);
         Mat conv;
         int _code;
         int _chs;
         if (code.getValue()==0) { _code=-1; _chs=-1; }
         else if (code.getValue()==1) { _code=-1; _chs=0; } 
         else if (code.getValue()==2) { _code=-1; _chs=1; } 
         else if (code.getValue()==3) { _code=-1; _chs=2; } 
         else if (code.getValue()==4) { _code=CV_BGR2HSV; _chs=0; } 
         else if (code.getValue()==5) { _code=CV_BGR2HSV; _chs=1; } 
         else if (code.getValue()==6) { _code=CV_BGR2HSV; _chs=2; } 
         else if (code.getValue()==7) { _code=CV_BGR2GRAY; _chs=-1; } 
         else if (code.getValue()==8) { _code=CV_BGR2YUV; _chs=0; } 
         else if (code.getValue()==9) { _code=CV_BGR2YUV; _chs=1; } 
         else if (code.getValue()==10) { _code=CV_BGR2YUV; _chs=2; }
         if (_code!=-1)
            cvtColor(original,conv,_code);
         else
            original.copyTo(conv);
         vector<Mat> ch;
         if (_chs!=-1) {
            split(conv,ch);
            return ch[_chs];
         } else {
            return conv;
         }
      }

      vector<ParamBase*> getParams() {
         vector<ParamBase*> params;
         params.push_back(&code);
         return params;
      }

      string getName() {
         return "show different color spaces";
      }
};

}
#endif
