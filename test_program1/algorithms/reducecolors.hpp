#ifndef __REDUCECOLORS_HPP__
#define __REDUCECOLORS_HPP__

#include <opencv2/opencv.hpp>
#include "opencv2/imgcodecs.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include <iostream>
#include <math.h>
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <dirent.h>
#include <queue>

#include "../algorithms_base.hpp"

using namespace std;
using namespace cv;

namespace my {

class ReduceColors : public Algorithm {
      Param<int> num;
   public:
      ReduceColors() :
         num("color bins",128,1,0,255) {
      }

      Mat convert(Mat original) {
         Mat result;
         original.copyTo(result);
	 for (int i=0; i < 3*original.cols; i++)
           for (int j=0; j< original.rows; j++)
              result.at<u_int8_t>(j,i)= original.at<u_int8_t>(j,i)/num.getValue()*num.getValue();
         return result;
      }

      vector<ParamBase*> getParams() {
         vector<ParamBase*> params;
         params.push_back(&num);
         return params;
      }

      string getName() {
         return "color reduce";
      }
};

}
#endif
