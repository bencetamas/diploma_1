#ifndef __BILATERALFILTER_HPP__
#define __BILATERALFILTER_HPP__

#include <opencv2/opencv.hpp>
#include "opencv2/imgcodecs.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include <iostream>
#include <math.h>
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <dirent.h>
#include <queue>

#include "../algorithms_base.hpp"

using namespace std;
using namespace cv;

namespace my {

class EdgePreservingFilter : public Algorithm {
      Param<int> filterSize;
      Param<int> iterations;
      Param<int> sigmas;
      Param<float> sigmar;
      Param<int> openIter1;
      Param<int> openIter2;
   public:
      EdgePreservingFilter() :
         filterSize("Structuring element size",9,2,1,25),
         iterations("Iterations",1,1,1,30),
         sigmas("Sigma s",40,1,0,250),
         sigmar("Sigma r",0.5,0.1,0,250),
         openIter1("Open 1",2,1,0,200),
         openIter2("Open 2",0,1,0,200) {
      }

      Mat convert(Mat original) {
         Mat result;
result=original;
for (int i=0; i < iterations.getValue(); i++)
         edgePreservingFilter(result,result,1,sigmas.getValue(),sigmar.getValue());

/*Mat element = getStructuringElement( MORPH_RECT, Size( 2*openIter1.getValue()+1, 2*openIter1.getValue()+1 ) );
for (int i=0; i < openIter2.getValue(); i++)
   morphologyEx( result, result, 3, element );*/
         
Mat tmp;
Canny(result,tmp,100,200,3);
result=tmp;
 //detailEnhance(original,result,sigmas.getValue(),sigmar.getValue());
//stylization(original,result,sigmas.getValue(),sigmar.getValue());
         return result;
      }

      vector<ParamBase*> getParams() {
         vector<ParamBase*> params;
         params.push_back(&filterSize);
         params.push_back(&iterations);
         params.push_back(&sigmas);
         params.push_back(&sigmar);
         //params.push_back(&openIter1);
         //params.push_back(&openIter2);
         return params;
      }

      string getName() {
         return "edge pres fil.";
      }
};

class FindLine : public Algorithm {
      Param<int> filterSize;
      Param<int> iterations;
      Param<int> sigmas;
      Param<float> sigmar;
      Param<int> openIter1;
      Param<int> openIter2;
   public:
      FindLine() :
         filterSize("Structuring element size",9,2,1,25),
         iterations("Iterations",1,1,1,30),
         sigmas("Sigma s",40,1,0,250),
         sigmar("Sigma r",0.5,0.1,0,250),
         openIter1("Open 1",2,1,0,200),
         openIter2("Open 2",0,1,0,200) {
      }

      Point getPoint(Point p, int dir) {
         if (dir==0) return Point(p.x-1,p.y);
         if (dir==1) return Point(p.x-1,p.y+1);
         if (dir==2) return Point(p.x,p.y+1);
         if (dir==3) return Point(p.x+1,p.y+1);
         if (dir==4) return Point(p.x+1,p.y);
         if (dir==5) return Point(p.x+1,p.y-1);
         if (dir==6) return Point(p.x,p.y-1);
         if (dir==7) return Point(p.x-1,p.y-1);
         return Point(-1,-1);
      }

      bool validPoint(Point p, Mat img) {
         return (p.x > 0) && (p.y > 0) && (p.x < img.cols) && (p.y < img.rows);
      }

      bool is_line_(Mat img,Point start, int dir, vector<Point>& end, int min_length, int dir1c, int dir2c, int dir3c, Point current, int length) {
         bool a1,a2,a3;
         a1=false;a2=false;a3=false;
//cout << "Start (" << start.x << "," << start.y << ")" << endl;
         Point next=getPoint(current,dir);
         if (validPoint(next,img)) {
//cout << "Next point: (" << next.x << "," << next.y << ") dir" << dir << endl;
            if (img.at<unsigned char>(next.y,next.x)==255) 
               a1=is_line_(img,start,dir,end,min_length,dir1c,dir2c+1,dir3c,next,length+1);
            if (!a1 && length>=min_length && dir2c >= dir1c && dir2c >= dir3c && (dir1c ==0 || dir3c==0)) {
//cout << "Find end!" << "(" << start.x << "," << start.y << ");(" << current.x << "," << current.y << ")"  << endl;
               a1=true;
               end.push_back(current);
            }
         }

         next=getPoint(current,(dir-1)%8);
         if (validPoint(next,img)) {
//cout << "Next point: (" << next.x << "," << next.y << ")" << endl;
            if (img.at<unsigned char>(next.y,next.x)==255) 
               a2=is_line_(img,start,dir,end,min_length,dir1c+1,dir2c,dir3c,next,length+1);
            if (!a2 && length>=min_length && dir2c >= dir1c && dir2c >= dir3c && (dir1c ==0 || dir3c==0)) {
               a2=true;
//cout << "Find end!" << endl;
               end.push_back(current);
            }
         }

         next=getPoint(current,(dir+1)%8);
         if (validPoint(next,img)) {
//cout << "Next point: (" << next.x << "," << next.y << ")" << endl;
            if (img.at<unsigned char>(next.y,next.x)==255) 
               a3=is_line_(img,start,dir,end,min_length,dir1c,dir2c,dir3c+1,next,length+1);
            if (!a3 && length>=min_length && dir2c >= dir1c && dir2c >= dir3c && (dir1c ==0 || dir3c==0)) {
               a3=true;
//cout << "Find end!" << endl;
               end.push_back(current);
            }
         }

         return a1 || a2 || a3;
      }

      bool is_line(Mat img,Point start, int dir, vector<Point>& end, int min_length) {
         return is_line_(img,start,dir,end,min_length,0,0,0,start,0);
      }

      Mat convert(Mat original) {
         Mat result;
result=original;
for (int i=0; i < iterations.getValue(); i++)
         edgePreservingFilter(result,result,1,sigmas.getValue(),sigmar.getValue());


/*Mat element = getStructuringElement( MORPH_RECT, Size( 2*openIter1.getValue()+1, 2*openIter1.getValue()+1 ) );
for (int i=0; i < openIter2.getValue(); i++)
   morphologyEx( result, result, 3, element );*/
         
Mat tmp;
Canny(result,tmp,100,200,3);
result=tmp;

Mat element = getStructuringElement( MORPH_RECT, Size( 2*1+1, 1 ) );
for (int i=0; i < 1; i++)
   dilate( result, result, element );

vector<Point> line_start;
vector<Point> line_end;/*
for (int x=0; x< 300/*result.cols*; x++)
   //for (int y=0; y < result.rows; y++) {
   for (int y=170; y < 200; y++) {
      if (result.at<unsigned char>(y,x) == 255) {

cout << "Processing point (" << x << "," << y << ")" << endl;
         vector<Point> end;
         for (int dir=0; dir < 8 ; dir++)
            if (is_line(result,Point(x,y),dir,end,100)) 
               for (int i=0; i<end.size(); i++) {
                  line_start.push_back(Point(x,y));
                  line_end.push_back(end[i]);
               }
      }
   }*/

cvtColor(result,result,CV_GRAY2BGR);
RNG rng;
line(result,Point(0,170),Point(100,170),Scalar(0,0,255));
line(result,Point(0,200),Point(100,200),Scalar(0,0,255));
cout << line_start.size() << endl;
for (int i=0 ; i<line_start.size(); i++) {
   if (rng.uniform(0,line_start.size()) < line_start.size()/100)
   line(result,line_start[i],line_end[i],Scalar(0,0,255));
}
 //detailEnhance(original,result,sigmas.getValue(),sigmar.getValue());
//stylization(original,result,sigmas.getValue(),sigmar.getValue());
         return result;
      }

      vector<ParamBase*> getParams() {
         vector<ParamBase*> params;
         params.push_back(&filterSize);
         params.push_back(&iterations);
         params.push_back(&sigmas);
         params.push_back(&sigmar);
         //params.push_back(&openIter1);
         //params.push_back(&openIter2);
         return params;
      }

      string getName() {
         return "edge pres fil.";
      }
};

class BilateralFilter : public Algorithm {
      Param<int> filterSize;
      Param<int> iterations;
      Param<int> sigmaColor;
      Param<int> sigmaSpace;
      Param<int> openIter1;
      Param<int> openIter2;
   public:
      BilateralFilter() :
         filterSize("Structuring element size",9,2,1,25),
         iterations("Iterations",21,1,1,30),
         sigmaColor("Sigma color",45,1,0,250),
         sigmaSpace("Sigma space",20,1,0,250),
         openIter1("Open 1",2,1,0,200),
         openIter2("Open 2",0,1,0,200) {
      }

      Mat convert(Mat original) {
/*
Mat rs,rs2;
Rect r(0,original.rows-40,40,40);
Mat tmp(original,r);
tmp.copyTo(rs);
//resize(original,rs,Size(original.cols/32,original.rows/32),INTER_NEAREST);
resize(rs,rs2,Size(original.cols,original.rows),INTER_NEAREST);
return rs2;*/
 Mat result;
         original.copyTo(result);
/*Mat element = getStructuringElement( MORPH_RECT, Size( 2*openIter1.getValue()+1, 2*openIter1.getValue()+1 ) );
for (int i=0; i < openIter2.getValue(); i++)
   morphologyEx( result, result, 3, element );*/
         /*Mat result;
         original.copyTo(result);
*/
         for (int i=0; i<iterations.getValue(); i++) {
            Mat tmp;
            bilateralFilter(result,tmp,filterSize.getValue(),sigmaColor.getValue(),sigmaSpace.getValue());
            tmp.copyTo(result);
            //adaptiveBilateralFilter(original,result,filterSize.getValue(),sigmaColor.getValue(),sigmaSpace.getValue());
         }
/*for (int i=0; i < openIter2.getValue(); i++)
   morphologyEx( result, result, 3, element );*/
/*Mat gray;
cvtColor(result,gray,CV_BGR2GRAY);
return gray;
 Mat element = getStructuringElement( MORPH_RECT, Size( 5, 5 ) );
for (int i=0; i < 5; i++)
   morphologyEx( result, result, 2, element );
return result;*/
         /*Mat tmp;
         Scharr(result,tmp,-1,1,0);
return tmp;*/
Mat canny;
Canny(result,canny,100,200,3);
result=canny;
         return result;
      }

      vector<ParamBase*> getParams() {
         vector<ParamBase*> params;
         params.push_back(&filterSize);
         params.push_back(&iterations);
         params.push_back(&sigmaColor);
         params.push_back(&sigmaSpace);
         //params.push_back(&openIter1);
         //params.push_back(&openIter2);
         return params;
      }

      string getName() {
         return "morphology operation";
      }
};

/*
class BilateralHoughLine : public Algorithm {
      BilateralFilter bif;
      CannyAndHoughLines hough;
   public:
      BilateralHoughLine() :
        bif(),
        hough()  {
      }

      Mat convert(Mat original) {
	 Mat tmp;
         tmp=bif.convert(original);
         return hough.convert(tmp);
      }

      vector<ParamBase*> getParams() {
         vector<ParamBase*> params;
         for (int i=0 ; i < hough.getParams().size(); i++)
	    params.push_back(hough.getParams()[i]);
         for (int i=0 ; i < bif.getParams().size(); i++)
	    params.push_back(bif.getParams()[i]);
         return params;
      }

      string getName() {
         return "bilateral filter, canny, hough line";
      }
};*/
}
#endif
