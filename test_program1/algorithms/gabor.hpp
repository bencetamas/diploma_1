#ifndef __GABOR_HPP__
#define __GABOR_HPP__

#include <opencv2/opencv.hpp>
#include "opencv2/imgcodecs.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include <iostream>
#include <math.h>
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <dirent.h>
#include <queue>

#include "../algorithms_base.hpp"

using namespace std;
using namespace cv;

namespace my {

class SimpleGabor : public Algorithm {
      Param<int> size;
      Param<double> sigma;
      Param<double> theta;
      Param<double> lambda;
      Param<double> gamma;
   public:
      SimpleGabor() :
         size("size",3,1,1,255),
         sigma("sigma",1.0,0.3,0.1,255.0),
         theta("theta",0.0,0.1,0.0,255.0),
         lambda("lambda",3.0,1.0,0.1,255.0),
         gamma("gamma",1.0,1.0,0.0,255.0) {
      }

      Mat convert(Mat original) {
         Mat result;
         Mat kernel=getGaborKernel(Size(size.getValue(),size.getValue()),sigma.getValue(),theta.getValue(),lambda.getValue(),gamma.getValue(), CV_PI*0.5, CV_64F);
         filter2D(original,result,-1,kernel);
         return result;
      }

      vector<ParamBase*> getParams() {
         vector<ParamBase*> params;
         params.push_back(&size);
         params.push_back(&sigma);
         params.push_back(&theta);
         params.push_back(&lambda);
         params.push_back(&gamma);
         return params;
      }

      string getName() {
         return "'pure' Canny edge detection algorithm";
      }
};

}
#endif
