#ifndef __DETECTLINE_HPP__
#define __DETECTLINE_HPP__

#include <opencv2/opencv.hpp>
#include "opencv2/imgcodecs.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include <iostream>
#include <math.h>
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <dirent.h>
#include <queue>

#include "../algorithms_base.hpp"

using namespace std;
using namespace cv;

namespace my {

class DetectLine : public Algorithm {
      Param<int> thres;
      Param<int> filterSize;
      Param<int> method;
      Param<int> method1;
      Param<int> method2;
   public:
      DetectLine() :
         thres("threshold",28,1,0,255),
         filterSize("filter size",30,2,1,200),
         method("method id",1,1,1,3),
         method1("method param 1(canny min)",125,1,1,255),
         method2("method param 2(canny max)",146,1,1,255) {
      }

      Mat convert(const Mat original) {
         Mat edge,tmp;
         if (method.getValue()==1) {
            cvtColor(original,tmp,CV_BGR2GRAY); 
            Canny(tmp,edge,method1.getValue(),method2.getValue(),3);
         } else if (method.getValue()==2) {
            Sobel(original,edge,CV_8U,1,0);
         } else if (method.getValue()==3) {
            Sobel(original,edge,CV_8U,0,1);
         }        
         
         vector<int> kx; 
         for (int i=0; i < filterSize.getValue(); i++) {
            kx.push_back(1);
         }
         vector<int> ky;
         ky.push_back(-1); ky.push_back(2); ky.push_back(-1);

         Mat result;
         sepFilter2D(edge,result,-1,kx,ky);

         return result;
      }

      vector<ParamBase*> getParams() {
         vector<ParamBase*> params;
         params.push_back(&thres);
         params.push_back(&filterSize);
         params.push_back(&method);
         params.push_back(&method1);
         params.push_back(&method2);
         return params;
      }

      string getName() {
         return "edge density";
      }
};

} 

#endif
