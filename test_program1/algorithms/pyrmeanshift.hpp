class PyrMeanShift : public Algorithm {
      Param<int> sp;
      Param<int> sr;
      Param<int> maxLevel;
      Param<int> maxIter;
      Param<double> eps;
      Param<int> filterSize;
   public:
      PyrMeanShift() :
         sp("spatial window radius",20,1,0,800),
         sr("color window radius",5,1,0,255),
         maxLevel("max level of pyramid segmentation",1,1,1,8),
         maxIter("max itaration number",5,1,1,20),
         eps("eps",1.0,0.1,0.1,5.0),
         filterSize("Blur filter size",3,2,1,15) {
      }

      Mat convert(Mat original) {

         Mat result,result2;
         medianBlur(original, result, filterSize.getValue());
         //original.copyTo(result);
cout << "test1" << endl;
         pyrMeanShiftFiltering(result,result2,sp.getValue(),sr.getValue(),maxLevel.getValue(),TermCriteria( TermCriteria::MAX_ITER+TermCriteria::EPS,maxIter.getValue(),eps.getValue()));
cout << "test2" << endl;
         return result2;
      }

      vector<ParamBase*> getParams() {
         vector<ParamBase*> params;
         params.push_back(&sp);
         params.push_back(&sr);
         params.push_back(&maxLevel);
         params.push_back(&maxIter);
         params.push_back(&eps);
         params.push_back(&filterSize);
         return params;
      }

      string getName() {
         return "pyramidal mean shift segmentation";
      }
};
