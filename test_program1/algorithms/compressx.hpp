#ifndef __COMPRESSX_HPP__
#define __COMPRESSX_HPP__

#include <opencv2/opencv.hpp>
#include "opencv2/imgcodecs.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include <iostream>
#include <math.h>
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <dirent.h>
#include <queue>

#include "../algorithms_base.hpp"
#include "./floodfill.hpp"

using namespace std;
using namespace cv;

namespace my {

class CompressX1 : public Algorithm {
      Param<int> ratio;
      Param<int> y;
      Param<int> cannyMinTreshold;
      Param<int> cannyMaxTreshold;
      Param<int> cannySize;
   public:
      CompressX1() :
         ratio("X resize ratio",35,1,0,1000),
         y("y",0,1,0,64),
         cannyMinTreshold("canny min tresh",100,1,0,255),
         cannyMaxTreshold("canny max tresh",200,1,0,255),
         cannySize("canny size",3,2,1,255) {
      }

      Mat convert(Mat original) {
         Mat result;
         normalize(original,original,0,255,NORM_MINMAX);
         //GaussianBlur(original,result,Size(ratio.getValue()*2+1,1),0);
         boxFilter(original,result,-1,Size(original.cols/ratio.getValue()*2+1,1/*,y.getValue()*2+1*/));
return result;
         //resize(original,result,Size(original.cols/ratio.getValue(),original.rows));
         //resize(result,result,Size(original.cols,original.rows));
         Mat tmp;
         Canny(result,tmp,cannyMinTreshold.getValue(),cannyMaxTreshold.getValue(),cannySize.getValue());

//cvtColor(result,result,CV_BGR2GRAY);
//Sobel(result,tmp,-1,0,1);
         result=tmp;
//threshold(result,result,cannyMaxTreshold.getValue(),255,THRESH_TOZERO);
         cvtColor(result,result,CV_GRAY2BGR);
tmp=result*0.8+original*0.2;
//resize(tmp,tmp,Size(),0.02,1.0,INTER_LINEAR);
//resize(tmp,tmp,Size(),50.0,1.0,INTER_LINEAR);
         return tmp;
      }

      vector<ParamBase*> getParams() {
         vector<ParamBase*> params;
         params.push_back(&ratio);
         params.push_back(&y);
         params.push_back(&cannyMinTreshold);
         params.push_back(&cannyMaxTreshold);
         params.push_back(&cannySize);
         return params;
      }

      string getName() {
         return "compressing in x direction";
      }
};

class CompressXY1 : public Algorithm {
      Param<int> ratiox;
      Param<int> ratioy;
      Param<int> cannyMinTreshold;
      Param<int> cannyMaxTreshold;
      Param<int> cannySize;
      Param<int> dilateXSize;
      Param<int> dilateYSize;

      vector<Rect> getCompressedFilteredBoundingRects(Mat& img, Size filterSize, Size dilateSize) {
         Mat tmp1,tmp2;
         boxFilter(img,tmp1,-1,filterSize);
         //GaussianBlur(img,tmp1,filterSize,0);
         Canny(tmp1,tmp2,cannyMinTreshold.getValue(),cannyMaxTreshold.getValue(),cannySize.getValue());

         Mat element = getStructuringElement( MORPH_RECT, dilateSize );
         dilate( tmp2, img, element );
         img.copyTo(tmp2);

         vector<vector<Point> > contours;
         vector<Vec4i> dummyHierarchy;
         findContours( tmp2, contours, dummyHierarchy, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_NONE );

         vector<Rect> rect;
         for (int i=0 ; i< contours.size(); i++) {
            rect.push_back(boundingRect(contours[i]));
         }

         return rect;
      }

      bool overlap(Rect r1, Rect r2) {
         return (r1 & r2).area() > 0;
      }
   public:
      CompressXY1() :
         ratiox("X resize ratio",35,1,0,1000),
         ratioy("Y resize ratio",50,1,0,1000),
         cannyMinTreshold("canny min tresh",100,1,0,255),
         cannyMaxTreshold("canny max tresh",200,1,0,255),
         cannySize("canny size",3,2,1,255),
         dilateXSize("dilate x",15,1,0,100),
         dilateYSize("dilate y",15,1,0,100) {
      }

      Mat convert(Mat original) {
         Mat result;
         normalize(original,original,0,255,NORM_MINMAX);
         edgePreservingFilter(original,original,1,40,0.5);
         // OR:  average with previous images?
         
         // Y DIRECTION
         Mat tmpY=original;
         vector<Rect> rectY=getCompressedFilteredBoundingRects(tmpY,
                    Size(1,original.rows/ratioy.getValue()*2+1),
                    Size( 2*dilateYSize.getValue()+1, 2*dilateYSize.getValue()+1 ));

         // X DIRECTION
         Mat tmpX=original;
         vector<Rect> rectX=getCompressedFilteredBoundingRects(tmpX,
                    Size(original.cols/ratiox.getValue()*2+1,1),
                    Size( 2*dilateXSize.getValue()+1, 2*dilateYSize.getValue()+1 ));

         Mat tmpS[]={tmpY,tmpX,Mat::zeros(tmpX.rows,tmpX.cols,tmpX.type())};
         Mat tmp;
         merge(tmpS,3,tmp);
         original=original*0.8+0.6*tmp;
         for (int ix=0; ix < rectX.size(); ix++)
            for (int iy=0; iy < rectY.size(); iy++) {
               if (overlap(rectX[ix],rectY[iy]) && (rectY[iy].y < rectX[ix].y)) {
                  rectangle(original,Rect(rectY[iy].tl(),Point(rectY[iy].x+rectY[iy].width,rectX[ix].y)),Scalar(0,0,255));
                  rectangle(original,rectX[ix],Scalar(0,255,255),3);
               }
            }
        return original;
/*
result=result*0.5+tmpx*0.3+tmpy*0.3;
         cvtColor(result,result,CV_GRAY2BGR);
result=result*0.8+original*0.2;
         return result;*/
      }

      vector<ParamBase*> getParams() {
         vector<ParamBase*> params;
         params.push_back(&ratiox);
         params.push_back(&ratioy);
         params.push_back(&cannyMinTreshold);
         params.push_back(&cannyMaxTreshold);
         params.push_back(&cannySize);
         params.push_back(&dilateXSize);
         params.push_back(&dilateYSize);
         return params;
      }

      string getName() {
         return "compressing in x direction";
      }
};

class CompressX1FloodFill : public Algorithm {
      Param<int> ratio;
      Param<int> y;
      Param<int> cannyMinTreshold;
      Param<int> cannyMaxTreshold;
      Param<int> cannySize;
      Param<int> ySampleCount;
      Param<int> yTranslate;
      Param<int> xTranslate;
      Param<double> xyRatio;
   public:
      CompressX1FloodFill() :
         ratio("X resize ratio",10,1,0,1000),
         y("y",0,1,0,64),
         cannyMinTreshold("canny min tresh",100,1,0,255),
         cannyMaxTreshold("canny max tresh",200,1,0,255),
         cannySize("canny size",3,2,1,255),
         ySampleCount("y sample count",6,1,1,100),
         yTranslate("y translate",7,1,1,50),
         xTranslate("x translate",5,1,1,50),
         xyRatio("xy ratio",40.0,0.1,0.2,50.0) {
      }

private:
RNG rng;

      vector<Rect> getVerticalMask(Mat& img, Size filterSize, Size dilateSize) {
         Mat tmp1,tmp2;
         boxFilter(img,tmp1,-1,filterSize);
         //GaussianBlur(img,tmp1,filterSize,0);
         Canny(tmp1,tmp2,100,200,3);
         Mat element = getStructuringElement( MORPH_RECT, dilateSize );
         dilate( tmp2, img, element );

         vector<vector<Point> > contours;
         vector<Vec4i> dummyHierarchy;
         img.copyTo(tmp1);
         findContours( tmp1, contours, dummyHierarchy, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_NONE );

         vector<Rect> rect;
         for (int i=0 ; i< contours.size(); i++) {
            rect.push_back(boundingRect(contours[i]));
         }

         return rect;
      }
public:

      bool findCandidate(Mat original, Point candidatePoint,Mat& returnMask) {
         FloodFill2 ffl=FloodFill2(candidatePoint.x,candidatePoint.y);
         Mat mask;
         bool reject=false;
         ffl.convert(original,reject,mask);
         if (!reject) {
            cout << "Find candidate!" << endl;

            Mat tmp2;
            original.copyTo(tmp2);
            vector<Rect> vMask=getVerticalMask(tmp2,Size(1,original.rows/35*2+1),Size(15,15));
            Mat tmp;
            cvtColor(mask,tmp,CV_BGR2GRAY);
            //cout << countNonZero(tmp) << endl;
            Mat element = getStructuringElement( MORPH_RECT, Size(15,15) );
            dilate( tmp, tmp, element );
            Rect fill=boundingRect(tmp);

            // Check if vertical and horizontal mask overlaps in the right way
            reject=true;
            for (int i=0; i<vMask.size();i++) {
               if ((vMask[i] & fill).area() > 0 && vMask[i].y < fill.y)
                 reject=false;
            }
            if (reject) {
               cout << "Rejected after floodfill, vertical mask criteria" << endl;
               returnMask= Mat::zeros(original.rows,original.cols,original.type());
               cvtColor(returnMask,returnMask,CV_BGR2GRAY);
               return false;
            }

            // Change mask color in result
            Mat lut(1,256,CV_8UC3);
            Vec3b c=Vec3b(rng.uniform(10,255),rng.uniform(100,255),rng.uniform(10,255));
            for (int i=0; i<256;i++)
               lut.at<Vec3b>(i)=c;
            lut.at<Vec3b>(0)=Vec3b(0,0,0);
            LUT(mask,lut,mask);
            //circle(mask,candidatePoint,2,Scalar(0,0,255),2);

            // Return result
            Canny(original,tmp,100,200,3);
            cvtColor(tmp,tmp,CV_GRAY2BGR);
            returnMask=mask;
            cvtColor(returnMask,returnMask,CV_BGR2GRAY);
            return true;
         }
         // Rejected:
         returnMask= Mat::zeros(original.rows,original.cols,original.type());
         cvtColor(returnMask,returnMask,CV_BGR2GRAY);
         return false;
      }

      Mat convert(Mat original) {
         Mat result;
         normalize(original,original,0,255,NORM_MINMAX);
         boxFilter(original,result,-1,Size(original.cols/ratio.getValue()*2+1,y.getValue()*2+1));
         Mat tmp;
         Canny(result,tmp,cannyMinTreshold.getValue(),cannyMaxTreshold.getValue(),cannySize.getValue());
         result=tmp;
         cvtColor(result,result,CV_GRAY2BGR);
cout << "Begining search..." << endl;
         original.copyTo(result);
         result=Mat::zeros(original.size(),original.type());
         vector<Mat> candidates;
         for (int x=original.cols/ySampleCount.getValue(); x < original.cols; x+=original.cols/ySampleCount.getValue()) {
            for (int y=0; y < original.rows; y++) {
               if (tmp.at<char>(y,x)!=0) {
                //  circle(original,Point(x,y),2,Scalar(0,0,255),2);
                  Mat tmp;
                  if (y>yTranslate.getValue())
                     if (findCandidate(original,Point(x,y-yTranslate.getValue()),tmp))
                        candidates.push_back(tmp);
                  if (y<original.rows-yTranslate.getValue())
                     if (findCandidate(original,Point(x,y+yTranslate.getValue()),tmp))
                        candidates.push_back(tmp);
                }
            }
         }
cout << "End of search..." << endl;
cout << "Found " << candidates.size() << " candidates." << endl;
         vector<bool> erased;
         for (int i=0; i<candidates.size();i++)
            erased.push_back(false);
         if (candidates.size()>0) {
            for (int i=0; i<candidates.size();i++)
               for (int j=0; j<candidates.size();j++) {
                  if (i!=j && !erased[i] && !erased[j]) {
                     if ((boundingRect(candidates[i]) & boundingRect(candidates[j])).area()>0) {
                        if (countNonZero(candidates[i]) > countNonZero(candidates[j]))
                           erased[j]=true;
                        else
                           erased[i]=true;
                     } 
                  }
               }
            for (int i=0; i<candidates.size();i++) {
              if (!erased[i]) {
                 Moments mom=moments(candidates[i]);
                 int x=mom.m10/mom.m00;
                 int y=mom.m01/mom.m00;
                 Mat mask;
                 candidates[i].copyTo(mask);

                 cvtColor(mask,mask,CV_GRAY2BGR);
                 Mat lut(1,256,CV_8UC3);
                 Vec3b c=Vec3b(rng.uniform(10,255),rng.uniform(100,255),rng.uniform(10,255));
                 for (int i=0; i<256;i++)
                    lut.at<Vec3b>(i)=c;
                 lut.at<Vec3b>(0)=Vec3b(0,0,0);
                 LUT(mask,lut,mask);
                 circle(mask,Point(x,y),2,Scalar(0,0,255),2);

                 result=result+mask;
              }
            }
         }
         
         return original*0.4+result*0.7;
      }

      vector<ParamBase*> getParams() {
         vector<ParamBase*> params;
         params.push_back(&ratio);
         params.push_back(&y);
         params.push_back(&cannyMinTreshold);
         params.push_back(&cannyMaxTreshold);
         params.push_back(&cannySize);
         params.push_back(&ySampleCount);
         params.push_back(&yTranslate);
         params.push_back(&xTranslate);
         params.push_back(&xyRatio);
         return params;
      }

      string getName() {
         return "compressing in x direction";
      }
};

}
#endif

