#ifndef __EDGEORIENTATION_HPP__
#define __EDGEORIENTATION_HPP__

#include <opencv2/opencv.hpp>
#include "opencv2/imgcodecs.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include <iostream>
#include <math.h>
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <dirent.h>
#include <queue>

#include "../algorithms_base.hpp"

using namespace std;
using namespace cv;

namespace my {

class EdgeOrientation : public Algorithm {
      Param<int> x1,x2,y1,y2;
      Param<int> bins;
   public:
      EdgeOrientation() :
         x1("x1",0,1,0,100),
         x2("x2",100,1,1,100),
         y1("y1",0,1,0,100),
         y2("y2",100,1,1,100),
         bins("bins",50,1,1,100) {
      }

      Mat convert(const Mat original) {
edgePreservingFilter(original,original,1,40,0.5);
         Mat o;
         cvtColor(original,o,CV_BGR2GRAY);
         Rect ROI(Point(x1.getValue()/100.0*original.cols,y1.getValue()/100.0*original.rows),Point(x2.getValue()/100.0*original.cols,y2.getValue()/100.0*original.rows));
         Mat roiImg=o(ROI);
         
         
         Mat dx,dy;
         Sobel(roiImg,dx,CV_32F,1,0);
         Sobel(roiImg,dy,CV_32F,0,1);
         //Scharr(roiImg,dx,CV_32F,1,0);
         //Scharr(roiImg,dy,CV_32F,0,1);

         Mat phases,magn;
         phase(dx,dy,phases,true);
         magnitude(dx,dy,magn);
         normalize(magn,magn,0,1.0,NORM_MINMAX);
         //normalize(phases,phases,0,1.0,NORM_MINMAX);

         int histBins[]={bins.getValue()};
         float phaseRanges[]={0,360.0};
         const float* ranges={phaseRanges};
         int channels[]={0};
         MatND hist;

         calcHist(&phases,1,channels,Mat(),hist,1,histBins,&ranges,true,false);
/*
double tmpMax;
minMaxLoc(magn, 0, &tmpMax, 0, 0);
cout << tmpMax << endl;
for (int i=10; i>0; i--) {
Mat mask;
threshold(magn,mask,0.9/i,1.0,THRESH_TOZERO);
normalize(mask,mask,0,255.0,NORM_MINMAX);
mask.convertTo(mask,CV_8UC3);
Mat tmp;
phases.copyTo(tmp);
phases=Mat::zeros(tmp.cols,tmp.rows,tmp.type());
tmp.copyTo(phases,mask);
if (i==0)
         calcHist(&phases,1,channels,Mat(),hist,1,histBins,&ranges,true,false);
else
         calcHist(&phases,1,channels,Mat(),hist,1,histBins,&ranges,true,true);
}
*/
         int scale = 10;
         Mat histImg = Mat::zeros(360,bins.getValue()*scale, original.type());

         double maxVal=0;
         minMaxLoc(hist, 0, &maxVal, 0, 0);
         for( int p = 0; p < bins.getValue(); p++ )
         {
            float binVal = hist.at<float>(p);
            int intensity = cvRound(binVal*900.0/ROI.area()/**360/maxVal*/);
            rectangle( histImg, Point(p*scale, 360-intensity),
                        Point( (p+1)*scale - 1, 360),
                        //Scalar::all(intensity),
                        Scalar(255,255,255),
                        CV_FILLED );
         }

         //namedWindow( "H-S Histogram", 1 );
         //imshow( "H-S Histogram", histImg );
         //waitKey();
         
         resize(histImg,histImg,Size(original.cols,original.rows));
         Mat origRect;
         original.copyTo(origRect);
         rectangle(origRect,Point(x1.getValue()/100.0*original.cols,y1.getValue()/100.0*original.rows),Point(x2.getValue()/100.0*original.cols,y2.getValue()/100.0*original.rows),Scalar(0,0,255));
         Mat result=origRect*0.6+histImg*0.4;
         resize(result,result,Size(original.cols,original.rows));
         return result;
      }

      vector<ParamBase*> getParams() {
         vector<ParamBase*> params;
         params.push_back(&x1);
         params.push_back(&x2);
         params.push_back(&y1);
         params.push_back(&y2);
         params.push_back(&bins);
         return params;
      }

      string getName() {
         return "edge orientation";
      }
};

} 

#endif
