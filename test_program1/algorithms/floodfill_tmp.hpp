#ifndef __FTMP_HPP__
#define __FTMP_HPP__

#include <opencv2/opencv.hpp>
#include "opencv2/imgcodecs.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include <iostream>
#include <math.h>
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <dirent.h>
#include <queue>

#include "../algorithms_base.hpp"

using namespace std;
using namespace cv;

namespace my {

class FloodFillTemp : public Algorithm {
      Param<int> y;
      Param<int> x;
      Param<int> diffX;
      Param<int> diffY;
      Param<int> filterSize;
      Param<int> maxGapX;
      Param<int> maxGapY;


      Vec3b diffX_() {
         return Vec3b(diffX.getValue(),diffX.getValue(),diffX.getValue());
      }

      Vec3b diffY_() {
         return Vec3b(diffY.getValue(),diffY.getValue(),diffY.getValue());
      }

      bool isGoodPoint(Vec3b seedC, Vec3b pC, Vec3b cDiff) {
         Vec3b d1=seedC-pC;
         Vec3b d2=pC-seedC;
         return d1[0]<cDiff[0] && d2[0] < cDiff[0] && 
                d1[1]<cDiff[1] && d2[1] < cDiff[1] &&
                d1[2]<cDiff[2] && d2[2] < cDiff[2];
      }

      bool isGoodPoint(vector<Vec3b> seedC, Vec3b pC, Vec3b cDiff) {
         for (int i=0; i < seedC.size();i++)
           if (isGoodPoint(seedC[i],pC,cDiff))
              return true;
         return false;
      }
   public:
      FloodFillTemp() :
         y("y coord (%*4)",200,1,0,400),
         x("x coord (%*4)",200,1,0,400),
         diffX("max color diff x",50,1,0,255),
         diffY("max color diff y",10,1,0,255),
         filterSize("morphology",1,1,1,15),
         maxGapX("Max gap x",1,1,0,80),
         maxGapY("Max gap y",1,1,0,80) {
      }

      Mat myFloodFill(Mat img,Point seed, Vec3b maskColor,bool useSeedColor,Vec3b& seedColor,int maxIter=-1) {
         if (!useSeedColor)
            seedColor=img.at<Vec3b>(seed.y,seed.x);
         Vec3f sumColor=Vec3f(0,0,0);
         Mat mask=Mat::zeros(img.size(),img.type());
	 queue<Point> scheduled;
         scheduled.push(seed);    
         Mat visitedMatX=Mat::zeros(img.size(),CV_8UC1);
         Mat visitedMatY=Mat::zeros(img.size(),CV_8UC1);
         visitedMatX.at<unsigned char>(seed.y,seed.x)=1;
         visitedMatY.at<unsigned char>(seed.y,seed.x)=1;
         int n=0;
     
         while (!scheduled.empty() && n!=maxIter) {
            Point curr=scheduled.front();
            scheduled.pop();
            Vec3b vtmp=img.at<Vec3b>(curr.y,curr.x);
            sumColor=sumColor+Vec3f(vtmp[0],vtmp[1],vtmp[2]);
            mask.at<Vec3b>(curr.y,curr.x)=maskColor;
            int x1=max(0,curr.x-maxGapX.getValue());
            int x2=min(img.cols-1,curr.x+maxGapX.getValue());
            int y1=max(0,curr.y-maxGapY.getValue());
            int y2=min(img.rows-1,curr.y+maxGapY.getValue());
            for (int x=x1; x<=x2; x++) {
              if (!visitedMatX.at<unsigned char>(curr.y,x) && isGoodPoint(seedColor,img.at<Vec3b>(curr.y,x),diffX_())) {
                 scheduled.push(Point(x,curr.y));
              }
              visitedMatX.at<unsigned char>(curr.y,x)=1;
            }
            for (int y=y1; y<=y2; y++) {
              if (!visitedMatY.at<unsigned char>(y,curr.x) && isGoodPoint(seedColor,img.at<Vec3b>(y,curr.x),diffY_())) {
                 scheduled.push(Point(curr.x,y));
              }
              visitedMatY.at<unsigned char>(y,curr.x)=1;                 
            }
            n++;
         }
         sumColor=sumColor/n;
         seedColor=Vec3b(int(sumColor[0]),int(sumColor[1]),int(sumColor[2]));

         return mask;
      }

      Mat convert(Mat original) {
         Mat result;

//BilateralFilter bif=BilateralFilter();
//result=bif.convert(original);
       //  medianBlur(original, result, filterSize.getValue());
         original.copyTo(result);
normalize(result,result,0,255,NORM_MINMAX);
vector<Mat> res_;
split(result,res_);
for (int i=0 ; i<res_.size() ;i++)
equalizeHist(res_[i],res_[i]);
//merge(res_,result);
         double x_=x.getValue()/400.0*result.size().width;
         double y_=y.getValue()/400.0*result.size().height;
         Vec3b seedColor;
vector<Vec3b> seedColorS;
for (int _x=x_-filterSize.getValue(); _x < x_+filterSize.getValue(); _x++)
for (int _y=y_-filterSize.getValue(); _y < y_+filterSize.getValue(); _y++) {
seedColorS.push_back(result.at<Vec3b>(_y,_x));
}

         Mat mask=myFloodFill(result,Point(x_,y_),Vec3b(128,100,128),false,seedColor,-1); ///maxiter=1000
         //mask=myFloodFill(result,Point(x_,y_),Vec3b(128,100,128),true,seedColor);
//Mat element = getStructuringElement( 0, Size( 2*filterSize.getValue() + 1, 2*filterSize.getValue()+1 ), Point( -1, -1 ) );
Mat element = getStructuringElement( 0, Size( 2*filterSize.getValue() + 1, 2*1+1 ), Point( -1, -1 ) );
/*for (int i=0; i < filterSize.getValue();i++)
dilate(mask,mask,element);
for (int i=0; i < filterSize.getValue();i++)
erode(mask,mask,element);*/
//morphologyEx( mask, mask, 3, element );

         vector<Point> shape;
         for (int x=0; x < mask.cols; x++)
            for (int y=0; y < mask.rows; y++)
               if (mask.at<Vec3b>(y,x)!=Vec3b(0,0,0))
                  shape.push_back(Point(x,y));
         if (shape.size()>0) {
            Vec4f line_;
            Mat gray;
            cvtColor(mask,gray,CV_BGR2GRAY);
            Moments mom=moments(gray,true);
            fitLine(shape,line_,CV_DIST_L2,0,0.01,0.01);
            double xc=mom.m10/mom.m00;
            double yc=mom.m01/mom.m00;
            double dx=line_[0]*boundingRect(shape).width;
            double dy=line_[1]*boundingRect(shape).width;
            double dx2=line_[1]*boundingRect(shape).height;
            double dy2=line_[0]*boundingRect(shape).height;
            line(mask,Point(xc-dx/2,yc-dy/2),Point(xc+dx/2,yc+dy/2),Scalar(0,50,255));
            line(mask,Point(xc-dx2/2,yc-dy2/2),Point(xc+dx2/2,yc+dy2/2),Scalar(0,50,255));
            rectangle(mask,boundingRect(shape),Scalar(200,0,0));
         }

         circle(mask,Point(x_,y_),1,Vec3b(255,255,30));
         return result*0.2+mask*0.8;
      }

      vector<ParamBase*> getParams() {
         vector<ParamBase*> params;
         params.push_back(&y);
         params.push_back(&x);
         params.push_back(&diffX);
         params.push_back(&diffY);
         params.push_back(&filterSize);
         params.push_back(&maxGapX);
         params.push_back(&maxGapY);
         return params;
      }

      string getName() {
         return "floodfill image segmentation own algorithm";
      }
};

}
#endif
