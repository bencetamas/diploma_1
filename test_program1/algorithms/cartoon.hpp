class Cartoon1 : public Algorithm {
      Param<int> filterSize;
      Param<int> iterations;
      Param<int> sigmaColor;
      Param<int> sigmaSpace;
   public:
      Cartoon1() :
         filterSize("Structuring element size",3,2,1,25),
         iterations("Iterations",1,1,1,30),
         sigmaColor("Sigma color",10,1,0,250),
         sigmaSpace("Sigma space",10,1,0,250) {
      }

      Mat convert(Mat original) {
         Mat result;
         original.copyTo(result);
         for (int i=0; i<iterations.getValue(); i++) {
            bilateralFilter(original,result,filterSize.getValue(),sigmaColor.getValue(),sigmaSpace.getValue());
            //adaptiveBilateralFilter(original,result,filterSize.getValue(),sigmaColor.getValue(),sigmaSpace.getValue());
         }
         return result;
      }

      vector<ParamBase*> getParams() {
         vector<ParamBase*> params;
         params.push_back(&filterSize);
         params.push_back(&iterations);
         params.push_back(&sigmaColor);
         params.push_back(&sigmaSpace);
         return params;
      }

      string getName() {
         return "cartoon";
      }
};
