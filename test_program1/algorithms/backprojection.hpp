class BackProjection : public Algorithm {
      Param<int> y;
      Param<int> x;
      Param<int> diffX;
      Param<int> diffY;
      Param<int> filterSize;
      Param<int> maxGapX;
      Param<int> maxGapY;
   public:
      BackProjection() :
         y("y coord (%*4)",200,1,0,400),
         x("x coord (%*4)",200,1,0,400),
         diffX("max color diff x",50,1,0,255),
         diffY("max color diff y",10,1,0,255),
         filterSize("Blur filter size",32,1,0,255),
         maxGapX("Max gap x",1,1,0,80),
         maxGapY("Max gap y",1,1,0,80) {
      }

      Mat convert(Mat original) {


         Mat hist;
         int histSize[] = {filterSize.getValue(),filterSize.getValue(),filterSize.getValue()};
         float range[] = { 0, 255 };
         const float* ranges[] = { range,range,range };
         int chs[]={0,1,2};
         const int* channels=chs;

Rect bgrR=Rect(original.cols/3,original.rows*3/4,original.cols/3,original.rows/4-1);
Mat bgr=Mat(original,bgrR);

         calcHist( &bgr, 1, channels, Mat(), hist, 3, histSize, ranges, true, false );
cout << "2" << endl;
//         normalize( hist, hist, 0, 255, NORM_MINMAX, -1, Mat() );
cout << "3" << endl;
         Mat backproj;
         calcBackProject( &original, 1, channels, hist, backproj, (const float**)ranges, 3, true );
normalize( backproj, backproj, 0, 255, NORM_MINMAX, -1, Mat() );
cout << "4" << endl;
rectangle(backproj,bgrR,Scalar(0,0,255));
         return backproj;
      }

      vector<ParamBase*> getParams() {
         vector<ParamBase*> params;
         params.push_back(&y);
         params.push_back(&x);
         params.push_back(&diffX);
         params.push_back(&diffY);
         params.push_back(&filterSize);
         params.push_back(&maxGapX);
         params.push_back(&maxGapY);
         return params;
      }

      string getName() {
         return "histogram back projection";
      }
};
