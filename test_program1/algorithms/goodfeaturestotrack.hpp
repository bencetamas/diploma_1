#ifndef __GOODFEATURESTOTRACK_HPP__
#define __GOODFEATURESTOTRACK_HPP__

#include <opencv2/opencv.hpp>
#include "opencv2/imgcodecs.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include <iostream>
#include <math.h>
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <dirent.h>
#include <queue>

#include "../algorithms_base.hpp"

using namespace std;
using namespace cv;

namespace my {

class GoodFeaturesToTrack : public Algorithm {
      Param<double> qualityLevel;
      Param<double> minDistance;
      Param<int> blockSize;
   public:
      GoodFeaturesToTrack() :
         qualityLevel("Quality level",0.01,0.002,0,2.0),
         minDistance("Minimum distance",5,1,0,200),
         blockSize("Block size",3,1,0,50) {
      }

      Mat convert(Mat original) {
         Mat gray;
         cvtColor(original,gray,CV_BGR2GRAY);
         vector<Point> corners;
         goodFeaturesToTrack(gray,corners,20000,qualityLevel.getValue(),minDistance.getValue(),noArray(),blockSize.getValue(),false);
         Mat result;
         original.copyTo(result);
         for (int i=0; i< corners.size(); i++) {
            circle(result,corners[i],3,Scalar(0,0,255));
	 }
         return result;
      }

      vector<ParamBase*> getParams() {
         vector<ParamBase*> params;
         params.push_back(&qualityLevel);
         params.push_back(&minDistance);
         params.push_back(&blockSize);
         return params;
      }

      string getName() {
         return "find good features to track";
      }
};

}
#endif
