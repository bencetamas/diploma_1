#ifndef __COLORBANK_HPP__
#define __COLORBANK_HPP__

#include <opencv2/opencv.hpp>
#include "opencv2/imgcodecs.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include <iostream>
#include <math.h>
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <dirent.h>
#include <queue>

#include "../algorithms_base.hpp"

using namespace std;
using namespace cv;

namespace my {

class Colorbank : public Algorithm {
      Param<int> radius;
      vector<Vec3b> colors;

      bool boatColor(Vec3b color) {
         for (int i=0; i < colors.size() ;i++) {
            if (abs(colors[i][0]-color[0]) <= radius.getValue() && 
                abs(colors[i][1]-color[1]) <= radius.getValue() && 
                abs(colors[i][2]-color[2]) <= radius.getValue())
               return true;
         }
         return false;
      }
   public:
      Colorbank() :
         radius("radius",1,1,1,200) {
         FileStorage fs=FileStorage("../test_program1/data/colorbank_test_individual_boat1.xml",FileStorage::READ);
         FileNode root=fs.root();
         root["colors"] >> colors;
         fs.release();
      }

      Mat convert(Mat original) {
         Mat tmp;
         normalize(original,tmp,0,255,NORM_MINMAX);
         Mat result;
         original.copyTo(result);

         for (int x=0; x<tmp.cols; x++)
            for (int y=0; y<tmp.rows; y++) 
               if (boatColor(tmp.at<Vec3b>(y,x)))
                  result.at<Vec3b>(y,x)=Vec3b(0,0,255);
         return result;
      }

      vector<ParamBase*> getParams() {
         vector<ParamBase*> params;
         params.push_back(&radius);
         return params;
      }

      string getName() {
         return "colorbank";
      }
};

class BladeColor : public Algorithm {
      Param<int> min;
      Param<int> max;
      Param<int> maxDiff;

      bool bladeColor(Vec3b color) {
         int mi=min.getValue();
         int m=max.getValue();
         int md=maxDiff.getValue();
         return color[0] < m && color[1] < m && color[2] < m &&
                color[0] > mi && color[1] > mi && color[2] > mi &&
                abs(color[0]-color[1]) < md && abs(color[0]-color[2]) < md && abs(color[1]-color[2]) < md;
      }
   public:
      BladeColor() :
         min("min",55,1,1,250), 
         max("max",85,1,1,250), 
         maxDiff("max diff",25,1,1,200) {
      }

      Mat convert(Mat original) {
         Mat tmp;
         //normalize(original,tmp,0,255,NORM_MINMAX);
tmp=original;
         Mat result;
         original.copyTo(result);

         for (int x=0; x<tmp.cols; x++)
            for (int y=0; y<tmp.rows; y++) 
               if (bladeColor(tmp.at<Vec3b>(y,x)))
                  result.at<Vec3b>(y,x)=Vec3b(0,0,255);
         return result;
      }

      vector<ParamBase*> getParams() {
         vector<ParamBase*> params;
         params.push_back(&min);
         params.push_back(&max);
         params.push_back(&maxDiff);
         return params;
      }

      string getName() {
         return "blade color filter";
      }
};

} 

#endif

