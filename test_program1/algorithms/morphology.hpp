class MorphologyFilter : public Algorithm {
      Param<int> filterSize;
      Param<int> opCode;
      Param<int> iterations;
   public:
      MorphologyFilter() :
         filterSize("Structuring element size",3,2,1,15),
         opCode("Operation code",0,1,0,6), 
         iterations("Iterations",1,1,1,30) {
      }

      Mat convert(Mat original) {
         Mat result;
         Mat element = getStructuringElement( MORPH_RECT, Size( 2*filterSize.getValue() + 1, 2*filterSize.getValue()+1 ) );
         original.copyTo(result);
         for (int i=0; i<iterations.getValue(); i++) {
            if (opCode.getValue()==0)
               erode(result, result, element);
            else if (opCode.getValue()==1)
               dilate(result,result,element);
            else
               morphologyEx( result, result, opCode.getValue(), element );
         }
         return result;
      }

      vector<ParamBase*> getParams() {
         vector<ParamBase*> params;
         params.push_back(&filterSize);
         params.push_back(&opCode);
         params.push_back(&iterations);
         return params;
      }

      string getName() {
         return "morphology operation";
      }
};
