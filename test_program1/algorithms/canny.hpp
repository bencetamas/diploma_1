#ifndef __CANNY_HPP__
#define __CANNY_HPP__

#include <opencv2/opencv.hpp>
#include "opencv2/imgcodecs.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include <iostream>
#include <math.h>
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <dirent.h>
#include <queue>

#include "../algorithms_base.hpp"

using namespace std;
using namespace cv;

namespace my {

class SimpleCanny : public Algorithm {
      Param<int> minTreshold;
      Param<int> maxTreshold;
   public:
      SimpleCanny() :
         minTreshold("Canny min. treshold",100,1,0,255),
         maxTreshold("Canny max. treshold",200,1,0,255) {
      }

      Mat convert(Mat original) {
         Mat result;
         Canny(original,result,minTreshold.getValue(),maxTreshold.getValue(),3);
         return result;
      }

      vector<ParamBase*> getParams() {
         vector<ParamBase*> params;
         params.push_back(&minTreshold);
         params.push_back(&maxTreshold);
         return params;
      }

      string getName() {
         return "'pure' Canny edge detection algorithm";
      }
};

class CannyWithBlur : public Algorithm {
      Param<int> minTreshold;
      Param<int> maxTreshold;
      Param<int> windowSize;
      Param<double> minSigma;
      Param<double> maxSigma;
   public:
      CannyWithBlur() :
         minTreshold("Canny min. treshold",100,1,0,255),
         maxTreshold("Canny max. treshold",200,1,0,255),
         windowSize("Gaussion blur win size",3,2,1,255),
         minSigma("Gauss min. sigma",1.0,0.3,0,255),
         maxSigma("Gauss max. sigma",1.0,0.3,0,255) {
      }

      Mat convert(Mat original) {
         cvtColor(original,original,CV_BGR2GRAY);
         Mat result;
         for (double sigma=minSigma.getValue(); sigma<=maxSigma.getValue(); sigma+=(maxSigma.getValue()-minSigma.getValue())/40.0+0.0001) {
            Mat gauss,canny;
            GaussianBlur(original,gauss,Size(windowSize.getValue(),windowSize.getValue()),sigma,0);
            Canny(gauss,canny,minTreshold.getValue(),maxTreshold.getValue(),3);
            if (result.empty())
               result=canny;
            else
               bitwise_or(result,canny,result);
         }
         return result;
      }

      vector<ParamBase*> getParams() {
         vector<ParamBase*> params;
         params.push_back(&minTreshold);
         params.push_back(&maxTreshold);
         params.push_back(&windowSize);
         params.push_back(&minSigma);
         params.push_back(&maxSigma);
         return params;
      }

      string getName() {
         return "multiple canny-gauss blur";
      }
};

}
#endif
