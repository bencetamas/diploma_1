#ifndef __FLOODFILL_HPP__
#define __FLOODFILL_HPP__

#include <opencv2/opencv.hpp>
#include "opencv2/imgcodecs.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include <iostream>
#include <math.h>
#include <cmath>
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <dirent.h>
#include <queue>

#include "../algorithms_base.hpp"

using namespace std;
using namespace cv;

namespace my {

class FloodFill : public Algorithm {
      Param<int> y;
      Param<int> x;
      Param<int> diff;
      Param<int> filterSize;
   public:
      FloodFill() :
         y("y coord (%*4)",200,1,0,400),
         x("x coord (%*4)",200,1,0,400),
         diff("max color diff",5,1,0,255),
         filterSize("Blur filter size",3,2,1,15) {
      }

      Mat convert(Mat original) {
         Scalar diff_=Scalar(diff.getValue(),diff.getValue(),diff.getValue());

         Mat result;
         medianBlur(original, result, filterSize.getValue());
         //original.copyTo(result);
         double x_=x.getValue()/400.0*result.size().width;
         double y_=y.getValue()/400.0*result.size().height;
         floodFill(result,Point(x_,y_),Scalar(128,255,128),0,diff_,diff_,4 | FLOODFILL_FIXED_RANGE);
         circle(result,Point(x_,y_),1,Scalar(0,0,255));
         return result;
      }

      vector<ParamBase*> getParams() {
         vector<ParamBase*> params;
         params.push_back(&y);
         params.push_back(&x);
         params.push_back(&diff);
         params.push_back(&filterSize);
         return params;
      }

      string getName() {
         return "floodfill image segmentation";
      }
};



class FloodFill2 : public Algorithm {
   public:
      Param<int> y;
      Param<int> x;
      Param<int> diffX;
      Param<int> diffY;
      Param<int> filterSize;
      Param<int> maxGapX;
      Param<int> maxGapY;

   private:
      Vec3b diffX_() {
         return Vec3b(diffX.getValue(),diffX.getValue(),diffX.getValue());
      }

      Vec3b diffY_() {
         return Vec3b(diffY.getValue(),diffY.getValue(),diffY.getValue());
      }

      bool isGoodPoint(Vec3b seedC, Vec3b pC, Vec3b cDiff) {
         Vec3b d1=seedC-pC;
         Vec3b d2=pC-seedC;
         return d1[0]<cDiff[0] && d2[0] < cDiff[0] && 
                d1[1]<cDiff[1] && d2[1] < cDiff[1] &&
                d1[2]<cDiff[2] && d2[2] < cDiff[2];
      }

      bool isGoodPoint(vector<Vec3b> seedC, Vec3b pC, Vec3b cDiff) {
         for (int i=0; i < seedC.size();i++)
           if (isGoodPoint(seedC[i],pC,cDiff))
              return true;
         return false;
      }
   public:
      FloodFill2() :
         y("y coord (%*4)",200,1,0,400),
         x("x coord (%*4)",200,1,0,400),
         diffX("max color diff x",50,1,0,255),
         diffY("max color diff y",10,1,0,255),
         filterSize("morphology",1,1,1,15),
         maxGapX("Max gap x",1,1,0,80),
         maxGapY("Max gap y",1,1,0,80) {
      }

      FloodFill2(int x,int y) :
         y("y coord (%*4)",200,1,0,400),
         x("x coord (%*4)",200,1,0,400),
         diffX("max color diff x",50,1,0,255),
         diffY("max color diff y",10,1,0,255),
         filterSize("morphology",1,1,1,15),
         maxGapX("Max gap x",25,1,0,80),
         maxGapY("Max gap y",1,1,0,80) {
         this->x=Param<int>("",x,1,0,4000);
         this->y=Param<int>("",y,1,0,4000);
      }

      Mat myFloodFill(Mat img,Point seed, Vec3b maskColor,bool useSeedColor,Vec3b& seedColor,int maxIter=-1) {
         if (!useSeedColor)
            seedColor=img.at<Vec3b>(seed.y,seed.x);
         Vec3f sumColor=Vec3f(0,0,0);
         Mat mask=Mat::zeros(img.size(),img.type());
	 queue<Point> scheduled;
         scheduled.push(seed);    
         Mat visitedMatX=Mat::zeros(img.size(),CV_8UC1);
         Mat visitedMatY=Mat::zeros(img.size(),CV_8UC1);
         visitedMatX.at<unsigned char>(seed.y,seed.x)=1;
         visitedMatY.at<unsigned char>(seed.y,seed.x)=1;
         int n=0;
     
         while (!scheduled.empty() && n!=maxIter) {
            Point curr=scheduled.front();
            scheduled.pop();
            Vec3b vtmp=img.at<Vec3b>(curr.y,curr.x);
            sumColor=sumColor+Vec3f(vtmp[0],vtmp[1],vtmp[2]);
            mask.at<Vec3b>(curr.y,curr.x)=maskColor;
            int x1=max(0,curr.x-maxGapX.getValue());
            int x2=min(img.cols-1,curr.x+maxGapX.getValue());
            int y1=max(0,curr.y-maxGapY.getValue());
            int y2=min(img.rows-1,curr.y+maxGapY.getValue());
            for (int x=x1; x<=x2; x++) {
              if (!visitedMatX.at<unsigned char>(curr.y,x) && isGoodPoint(seedColor,img.at<Vec3b>(curr.y,x),diffX_())) {
                 scheduled.push(Point(x,curr.y));
              }
              visitedMatX.at<unsigned char>(curr.y,x)=1;
            }
            for (int y=y1; y<=y2; y++) {
              if (!visitedMatY.at<unsigned char>(y,curr.x) && isGoodPoint(seedColor,img.at<Vec3b>(y,curr.x),diffY_())) {
                 scheduled.push(Point(curr.x,y));
              }
              visitedMatY.at<unsigned char>(y,curr.x)=1;                 
            }
            n++;
         }
         sumColor=sumColor/n;
         seedColor=Vec3b(int(sumColor[0]),int(sumColor[1]),int(sumColor[2]));

         return mask;
      }

      /** Parameters a little "tricky":
         - if reject=true, then the x,y parameters are treated as percentiles beetween 0 and 400, converted into real image coords
         - if reject=false, then the x,y parameters are treated as th real image coordiantes
      */
      Mat convert(Mat original, bool& reject, Mat& returnMask) {
         // Initialization
         bool convertCoords=false;
         if (reject)
            convertCoords=true;
         reject=false;
         Mat result;

         original.copyTo(result);
         normalize(result,result,0,255,NORM_MINMAX);

         /*vector<Mat> res_;
         split(result,res_);
         for (int i=0 ; i<res_.size() ;i++)
            equalizeHist(res_[i],res_[i]);
         merge(res_,result);*/

         double x_=x.getValue();
         double y_=y.getValue();
         if (convertCoords) {
            x_=x.getValue()/400.0*result.size().width;
            y_=y.getValue()/400.0*result.size().height;
         }

         // Doing the real work: FLOODFILL
         Vec3b seedColor;

         Mat mask=myFloodFill(result,Point(x_,y_),Vec3b(128,100,128),false,seedColor,-1); ///maxiter=1000
         mask.copyTo(returnMask);

         // Calculating shape descriptors
         vector<Point> shape;
         for (int x=0; x < mask.cols; x++)
            for (int y=0; y < mask.rows; y++)
               if (mask.at<Vec3b>(y,x)!=Vec3b(0,0,0))
                  shape.push_back(Point(x,y));
         if (shape.size()>0) {
            Vec4f line_;
            Mat gray;
            cvtColor(mask,gray,CV_BGR2GRAY);
            Moments mom=moments(gray,true);
            fitLine(shape,line_,CV_DIST_L2,0,0.01,0.01);
            double xc=mom.m10/mom.m00;
            double yc=mom.m01/mom.m00;
            double dx=line_[0]*boundingRect(shape).width;
            double dy=line_[1]*boundingRect(shape).width;
            double dx2=line_[1]*boundingRect(shape).height;
            double dy2=line_[0]*boundingRect(shape).height;
            line(mask,Point(xc-dx/2,yc-dy/2),Point(xc+dx/2,yc+dy/2),Scalar(0,50,255));
            line(mask,Point(xc-dx2/2,yc-dy2/2),Point(xc+dx2/2,yc+dy2/2),Scalar(0,50,255));
            rectangle(mask,boundingRect(shape),Scalar(200,0,0));

            // REJECT NOT GOOD SHAPES

            // Reject with angle
            std::ostringstream s; 
            double angle=atan(line_[1]/line_[0])/3.14*180;
            s << angle;
            putText(result,string("")+string(s.str()),Point(5,40),FONT_HERSHEY_SIMPLEX,1.0,Scalar(0,0,255));
            if (abs(angle) > 10.0) {
               putText(result,"Rejected angle.",Point(5,50),FONT_HERSHEY_SIMPLEX,0.3,Scalar(0,0,255));
               cout << "Rejected angle" << endl;
               reject=true;
            }

            // Reject with length-ratio
            double lengthratio=boundingRect(shape).width/boundingRect(shape).height;//mom.mu20/mom.mu02; 
            s.str("");s.clear(); 
            s << lengthratio ;//<< "mu20:" << mom.nu20 << "mu02:" << mom.nu02;
            putText(result,string("")+string(s.str()),Point(5,80),FONT_HERSHEY_SIMPLEX,1.0,Scalar(0,0,255));
            if (lengthratio < 10) {
               putText(result,"Rejected length-ratio.",Point(5,90),FONT_HERSHEY_SIMPLEX,0.3,Scalar(0,0,255));
               cout << "Rejected length" << endl;
               reject=true;
            }

            // Reject with length-ratio 2
            lengthratio=sqrt(mom.mu20/mom.mu02); 
            s.str("");s.clear(); 
            s << lengthratio ;//<< "mu20:" << mom.nu20 << "mu02:" << mom.nu02;
            putText(result,string("")+string(s.str()),Point(5,120),FONT_HERSHEY_SIMPLEX,1.0,Scalar(0,0,255));
            if (lengthratio < 10) {
               putText(result,"Rejected length-ratio2.",Point(5,130),FONT_HERSHEY_SIMPLEX,0.3,Scalar(0,0,255));
               cout << "Rejected length" << endl;
               reject=true;
            }

            // Reject with area 1
            Mat tmp;
            cvtColor(mask,tmp,CV_BGR2GRAY);
            double area=double(countNonZero(tmp))/mask.rows/mask.cols; 
            s.str("");s.clear(); 
            s << area ;//<< "mu20:" << mom.nu20 << "mu02:" << mom.nu02;
            putText(result,string("")+string(s.str()),Point(5,250),FONT_HERSHEY_SIMPLEX,0.8,Scalar(0,0,255));
            if (area > 0.1 || area < 0.0015) {
               putText(result,"Rejected area1.",Point(5,250),FONT_HERSHEY_SIMPLEX,0.3,Scalar(0,0,255));
               cout << "Rejected area" << endl;
               reject=true; 
            }
         }

         // HISTOGRAMS

         // Calculating foreground(fill) and background(not filled) histograms
         threshold(mask,mask,10,255,THRESH_BINARY);
         Mat mask2;
         cvtColor(mask,mask2,CV_BGR2GRAY);
         Mat invMask;
         threshold(mask2,invMask,10,255,THRESH_BINARY_INV);

         int histBins[]={50};
         float histRanges[]={1,255.0};
         const float* ranges={histRanges};
         int channels[]={1};
         MatND histForeground[3];
         MatND histBackground[3];

         // Foreground histogram
         Mat masked;
         original.copyTo(masked,mask);
         for (int i=0; i < 3 ;i++) {
            int channels[]={i};
            calcHist(&masked,1,channels,Mat(),histForeground[i],1,histBins,&ranges,true,false);
         }
         // Background histogram
         original.copyTo(masked,invMask);
         for (int i=0; i < 3 ;i++) {
            int channels[]={i};
            calcHist(&masked,1,channels,Mat(),histBackground[i],1,histBins,&ranges,true,false);
         }

         // Reject by histograms 1
         bool isNegCORREL=false;
         for (int i=0; i< 3; i++) {
            normalize( histForeground[i], histForeground[i], 0, 0.15, NORM_MINMAX, -1, Mat() );
            normalize( histBackground[i], histBackground[i], 0, 0.15, NORM_MINMAX, -1, Mat() );
            MatND tmp1,tmp2;
            normalize( histForeground[i], tmp1, 0, 1, NORM_MINMAX, -1, Mat() );
            normalize( histBackground[i], tmp2, 0, 1, NORM_MINMAX, -1, Mat() );
            double comp1=compareHist(tmp1,tmp2,CV_COMP_CORREL);
            double comp2=compareHist(tmp1,tmp2,CV_COMP_CHISQR);
            double comp3=compareHist(tmp1,tmp2,CV_COMP_INTERSECT);
            double comp4=compareHist(tmp1,tmp2,CV_COMP_BHATTACHARYYA);
            std::ostringstream s; 
            s << comp1 << ", " << comp2 << ", " << comp3 << ", " << comp4;
            putText(mask,string("")+string(s.str()),Point(5,160+i*20),FONT_HERSHEY_SIMPLEX,0.5,Scalar(0,0,255));
            if (comp1 < 0)
               isNegCORREL=true;
         }
         if (!isNegCORREL) {
            reject=true;
            putText(mask,"Rejected by color histogram.",Point(5,220),FONT_HERSHEY_SIMPLEX,0.4,Scalar(0,0,255));
            cout << "Rejected histogram" << endl;
         }

         // Show histograms
         int scale = 10;
         Mat histImg = Mat::zeros(255,histBins[0]*scale, original.type());

         // Show foreground histogram (bottom)
         for (int i=0; i<3; i++) {
            double maxVal=0;
            minMaxLoc(histForeground[i], 0, &maxVal, 0, 0);
            Scalar color=Scalar(0,0,0);
            color[i]=255;
            for( int p = 0; p < histBins[0]; p++ )
            {
               float binVal = histForeground[i].at<float>(p);
               int intensity = cvRound(binVal*255/*/countNonZero(mask2)/**360/maxVal*/);
               rectangle( histImg, Point(p*scale, 255-intensity),
                           Point( (p+1)*scale - 1, 255),
                           //Scalar::all(intensity),
                           color,
                           CV_FILLED );
            }
         }

         // Show background histogram(middle)
         for (int i=0; i<3; i++) {
            double maxVal=0;
            minMaxLoc(histBackground[i], 0, &maxVal, 0, 0);
            Scalar color=Scalar(0,0,0);
            color[i]=255;
            for( int p = 0; p < histBins[0]; p++ )
            {
               float binVal = histBackground[i].at<float>(p);
               int intensity = cvRound(binVal*255/*/countNonZero(invMask)/**360/maxVal*/);
               rectangle( histImg, Point(p*scale, 180-intensity),
                           Point( (p+1)*scale - 1, 180),
                           //Scalar::all(intensity),
                           color,
                           CV_FILLED );
            }
         }

         // Return image with original+floodfill mask+histograms
         resize(histImg,histImg,Size(original.cols,original.rows));

         circle(mask,Point(x_,y_),1,Vec3b(255,255,30));
         return result*0.2+mask*0.8+0.5*histImg;
      }

      Mat convert(Mat original) {
         bool dummy=true;
         Mat mask;
         return convert(original,dummy,mask);
      }

      vector<ParamBase*> getParams() {
         vector<ParamBase*> params;
         params.push_back(&y);
         params.push_back(&x);
         params.push_back(&diffX);
         params.push_back(&diffY);
         params.push_back(&filterSize);
         params.push_back(&maxGapX);
         params.push_back(&maxGapY);
         return params;
      }

      string getName() {
         return "floodfill image segmentation own algorithm";
      }
};

class FloodFill3 : public Algorithm {
   public:
      Param<int> y;
      Param<int> x;
      Param<int> diffX;
      Param<int> diffY;
      Param<int> filterSize;
      Param<int> maxGapX;
      Param<int> maxGapY;

   private:
      Vec3b diffX_() {
         return Vec3b(diffX.getValue(),diffX.getValue(),diffX.getValue());
      }

      Vec3b diffY_() {
         return Vec3b(diffY.getValue(),diffY.getValue(),diffY.getValue());
      }

      bool isGoodPoint(Vec3b seedC, Vec3b pC, Vec3b cDiff) {
         Vec3b d1=seedC-pC;
         Vec3b d2=pC-seedC;
         return d1[0]<cDiff[0] && d2[0] < cDiff[0] && 
                d1[1]<cDiff[1] && d2[1] < cDiff[1] &&
                d1[2]<cDiff[2] && d2[2] < cDiff[2];
      }

      bool isGoodPoint(vector<Vec3b> seedC, Vec3b pC, Vec3b cDiff) {
         for (int i=0; i < seedC.size();i++)
           if (isGoodPoint(seedC[i],pC,cDiff))
              return true;
         return false;
      }
   public:
      FloodFill3() :
         y("y coord",200,1,0,4000),
         x("x coord",200,1,0,4000),
         diffX("max color diff x",50,1,0,255),
         diffY("max color diff y",10,1,0,255),
         filterSize("morphology",1,1,1,15),
         maxGapX("Max gap x",1,1,0,80),
         maxGapY("Max gap y",1,1,0,80) {
      }

      FloodFill3(int x,int y) :
         y("y coord",200,1,0,4000),
         x("x coord",200,1,0,4000),
         diffX("max color diff x",50,1,0,255),
         diffY("max color diff y",10,1,0,255),
         filterSize("morphology",1,1,1,15),
         maxGapX("Max gap x",8,1,0,80),
         maxGapY("Max gap y",1,1,0,80) {
         this->x=Param<int>("",x,1,0,4000);
         this->y=Param<int>("",y,1,0,4000);
      }

      Mat myFloodFill(Mat img,Point seed, Vec3b maskColor,bool useSeedColor,Vec3b& seedColor,int maxIter=-1) {
         if (!useSeedColor)
            seedColor=img.at<Vec3b>(seed.y,seed.x);
         Vec3f sumColor=Vec3f(0,0,0);
         Mat mask=Mat::zeros(img.size(),img.type());
	 queue<Point> scheduled;
         scheduled.push(seed);    
         Mat visitedMatX=Mat::zeros(img.size(),CV_8UC1);
         Mat visitedMatY=Mat::zeros(img.size(),CV_8UC1);
         visitedMatX.at<unsigned char>(seed.y,seed.x)=1;
         visitedMatY.at<unsigned char>(seed.y,seed.x)=1;
         int n=0;
     
         while (!scheduled.empty() && n!=maxIter) {
            Point curr=scheduled.front();
            scheduled.pop();
            Vec3b vtmp=img.at<Vec3b>(curr.y,curr.x);
            sumColor=sumColor+Vec3f(vtmp[0],vtmp[1],vtmp[2]);
            mask.at<Vec3b>(curr.y,curr.x)=maskColor;
            int x1=max(0,curr.x-maxGapX.getValue());
            int x2=min(img.cols-1,curr.x+maxGapX.getValue());
            int y1=max(0,curr.y-maxGapY.getValue());
            int y2=min(img.rows-1,curr.y+maxGapY.getValue());
            for (int x=x1; x<=x2; x++) {
              if (!visitedMatX.at<unsigned char>(curr.y,x) && isGoodPoint(seedColor,img.at<Vec3b>(curr.y,x),diffX_())) {
                 scheduled.push(Point(x,curr.y));
              }
              visitedMatX.at<unsigned char>(curr.y,x)=1;
            }
            for (int y=y1; y<=y2; y++) {
              if (!visitedMatY.at<unsigned char>(y,curr.x) && isGoodPoint(seedColor,img.at<Vec3b>(y,curr.x),diffY_())) {
                 scheduled.push(Point(curr.x,y));
              }
              visitedMatY.at<unsigned char>(y,curr.x)=1;                 
            }
            n++;
         }
         sumColor=sumColor/n;
         seedColor=Vec3b(int(sumColor[0]),int(sumColor[1]),int(sumColor[2]));

         return mask;
      }

      Mat convert(Mat original) {
         Mat result;

         original.copyTo(result);
         normalize(result,result,0,255,NORM_MINMAX);

         double x_=x.getValue();
         double y_=y.getValue();
         Vec3b seedColor;

         Mat mask=myFloodFill(result,Point(x_,y_),Vec3b(128,100,128),false,seedColor,-1); ///maxiter=1000
 return mask;     
         vector<Point> shape;
         for (int x=0; x < mask.cols; x++)
            for (int y=0; y < mask.rows; y++)
               if (mask.at<Vec3b>(y,x)!=Vec3b(0,0,0))
                  shape.push_back(Point(x,y));
         if (shape.size()>0) {
            Vec4f line_;
            Mat gray;
            cvtColor(mask,gray,CV_BGR2GRAY);
            Moments mom=moments(gray,true);
            fitLine(shape,line_,CV_DIST_L2,0,0.01,0.01);
            double xc=mom.m10/mom.m00;
            double yc=mom.m01/mom.m00;
            double dx=line_[0]*boundingRect(shape).width;
            double dy=line_[1]*boundingRect(shape).width;
            double dx2=line_[1]*boundingRect(shape).height;
            double dy2=line_[0]*boundingRect(shape).height;
            line(mask,Point(xc-dx/2,yc-dy/2),Point(xc+dx/2,yc+dy/2),Scalar(0,50,255));
            line(mask,Point(xc-dx2/2,yc-dy2/2),Point(xc+dx2/2,yc+dy2/2),Scalar(0,50,255));
            rectangle(mask,boundingRect(shape),Scalar(200,0,0));
         }

         circle(mask,Point(x_,y_),1,Vec3b(255,255,30));
         return result*0.2+mask*0.8;
      }

      vector<ParamBase*> getParams() {
         vector<ParamBase*> params;
         params.push_back(&y);
         params.push_back(&x);
         params.push_back(&diffX);
         params.push_back(&diffY);
         params.push_back(&filterSize);
         params.push_back(&maxGapX);
         params.push_back(&maxGapY);
         return params;
      }

      string getName() {
         return "floodfill image segmentation own algorithm";
      }
};

} 

#endif
