#ifndef __CANNYANDLINEEXTRACT_HPP__
#define __CANNYANDLINEEXTRACT_HPP__

#include <opencv2/opencv.hpp>
#include "opencv2/imgcodecs.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include <iostream>
#include <math.h>
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <dirent.h>
#include <queue>

#include "../algorithms_base.hpp"

using namespace std;
using namespace cv;

namespace my {

class CannyAndLineExtract : public Algorithm {
      Param<int> minTreshold;
      Param<int> maxTreshold;
   public:
      CannyAndLineExtract() :
         minTreshold("Canny min. treshold",0,1,0,255),
         maxTreshold("Canny max. treshold",3,1,0,255) {
      }

      Mat convert(Mat original) {
//edgePreservingFilter(original,original,1,40,0.5);
         Mat result;
         Canny(original,result,minTreshold.getValue(),maxTreshold.getValue(),3);
         /*Mat elem=getStructuringElement(MORPH_RECT,Size(30,1));
         erode(result,result,elem);*/
         Mat elem=Mat::zeros(3,3,CV_8U);
         elem.at<unsigned char>(1,0)=255;
         elem.at<unsigned char>(1,1)=255;
         elem.at<unsigned char>(1,2)=255;
         erode(result,result,elem);
         //erode(result,result,elem);
         return result;   
      }

      vector<ParamBase*> getParams() {
         vector<ParamBase*> params;
         params.push_back(&minTreshold);
         params.push_back(&maxTreshold);
         return params;
      }

      string getName() {
         return "'pure' Canny edge detection algorithm";
      }
};



}
#endif
