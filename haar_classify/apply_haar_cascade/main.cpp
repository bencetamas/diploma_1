#include <opencv2/opencv.hpp>
#include "opencv2/imgcodecs.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include <iostream>
#include <math.h>
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <dirent.h>
#include <queue>
#include <fstream>

using namespace cv;
using namespace std;


int main(int argc, char** argv )
{
    if (argc < 4) {
       cout << "Not enough parameter" << endl;
       cout << "main <video-in> <classifier-cascade> <video-out>" << endl;
       return 1;
    }

    VideoCapture video(argv[1]);
    if (!video.isOpened()) {
       cout << "Cannot open file." << endl;
       return 2;
    }   

    Mat frame;
    CascadeClassifier cascade(argv[2]);
    video >> frame;

    VideoWriter vout(argv[3],VideoWriter::fourcc('M','P','E','G'),30,frame.size(),true);
    while (!frame.empty()) {
       //Mat tmp;
       //normalize(frame,tmp,0,255,NORM_MINMAX);
       Mat gray;
       cvtColor(frame,gray,CV_BGR2GRAY);

       vector<Rect> faces;
       //cascade.detectMultiScale(gray,faces,1.25,30);
       cascade.detectMultiScale(gray,faces,1.25,5);
       for (int i=0; i<faces.size(); i++)
          rectangle(frame,faces[i],Scalar(0,0,255),2);
       imshow("Detecting...",frame);
       char command=waitKey(10);
       if (command=='s')
          for (int j=0; j < 30*20; j++)
             video.grab();
       if (command=='e')
          break;
       vout << frame;
       video >> frame;
    }
    vout.release();
    
    return 0;
}

