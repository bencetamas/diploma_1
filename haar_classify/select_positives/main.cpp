#include <opencv2/opencv.hpp>
#include "opencv2/imgcodecs.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include <iostream>
#include <math.h>
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <dirent.h>
#include <queue>
#include <fstream>

using namespace cv;
using namespace std;

string windowName="Selecting positive image parts";

class ImageSequence{
      RNG rng;
      vector<string> files;
      int nextFileInd;
      bool videos;

      VideoCapture video;
      int currFrame;
      int frameCount;
      Mat frame; // current img

      vector<string> getFileList(string dir) {
	DIR *dpdf;
	struct dirent *epdf;
 
	dpdf = opendir(dir.c_str());
	if (dpdf != NULL) {
	   vector<string> result;
	   int i=0;
	   while (epdf = readdir(dpdf)) {
              if (string(epdf->d_name)=="." || string(epdf->d_name)=="..")
                 continue;
	      result.push_back(string(dir)+string(epdf->d_name));
	      i++;  
	   }
	  
	   closedir(dpdf);
	   return result;
	}
	
	return vector<string>();
      }
   public:
      ImageSequence(string dir,bool videos=false) :
        rng(RNG()) {
         files=getFileList(dir);
         this->videos=videos;
         nextFileInd=0;
  
         currFrame=0;
         frameCount=-1;
      }

      bool isNextImage() {
         return nextFileInd<files.size() || (videos && currFrame < frameCount-1);
      }

      void skip(int num) {
        if (videos) {
           for (; num > 0; num--) {
              if (currFrame >= frameCount-1) {
                video=VideoCapture(files[nextFileInd]);
                nextFileInd++;
                currFrame=-1;
                frameCount=video.get(CAP_PROP_FRAME_COUNT);
              }
              video.grab();
           }
        }
      }

      Mat getNextImage() {
         if (videos) {
            if (currFrame >= frameCount-1) {
               video=VideoCapture(files[nextFileInd]);
               nextFileInd++;
               currFrame=-1;
               frameCount=video.get(CAP_PROP_FRAME_COUNT);
            }
            video >> frame;
            currFrame++;
            return frame;
         } else {
            Mat img=imread(files[nextFileInd]);
            nextFileInd++;
            frame=img;
            return img;
         }
      }
  
      Mat flipCurrentImage() {
         flip(frame,frame,1);
         return frame;
      }

      Mat getNextVideo() {
         if (videos) {
            video=VideoCapture(files[nextFileInd]);
            nextFileInd++;
            currFrame=-1;
            frameCount=video.get(CAP_PROP_FRAME_COUNT);

            Mat frame;
            video >> frame;
            currFrame++;
            return frame;
         } 
      }
};

class Selection {
      int x1,y1,x2,y2;
      Mat img;


      void addToSelection(vector<Rect>& rects, Rect toBeAdded) {
         if (toBeAdded.height < 30 || toBeAdded.width < 30) 
            return;
         rects.push_back(toBeAdded);
      }
   public:
      Selection(Mat _img) :
        x1(-1),
        y1(-1),
        x2(-1),
        y2(-1),
        img(_img) {
      }

      Selection(const Selection& s) :
         x1(s.x1),
         x2(s.x2),
         y1(s.y1),
         y2(s.y2) {
      }

      ~Selection() {
         img.release();
      }

      bool isValid() {
         return x1>=0 && x2>=0 && y1 >=0 && y2>=0;
      }

      Point getTopLeft() {
         return Point(x1,y1);
      }

      Point getWidthHeight() {
         return Point(x2-x1,y2-y1);
      }

      void setTopLeft(int x, int y) {
         x1=x;
         y1=y;
      }

      void setBottomRight(int x, int y) {
         x2=x;
         y2=y;
         if (x1>x2) {
            int tmp=x1;
            x1=x2;
            x2=tmp;
         }
         if (y1>y2) {
            int tmp=y1;
            y1=y2;
            y2=tmp;
         }
      }
 
      Rect getSelection() {
         return Rect(Point(x1,y1),Point(x2,y2));
      }

      vector<Rect> getNonSelections() {
         vector<Rect> result;
         addToSelection(result,Rect(Point(0,0),Point(x1-1,img.size().height-1)));
         addToSelection(result,Rect(Point(0,0),Point(img.size().width-1,y1-1)));
         addToSelection(result,Rect(Point(x2+1,0),Point(img.size().width-1,img.size().height-1)));
         addToSelection(result,Rect(Point(0,y2+1),Point(img.size().width-1,img.size().height-1)));
         return result;
      }

      Mat getImage() {
         Mat tmp;
         img.copyTo(tmp);
         if (isValid()) 
            rectangle(tmp,getSelection(),Scalar(0,0,255),2);
         return tmp;
      }
};

void drawMultipleSelections(vector<Selection> positives,Selection* current, Mat img) {
   Mat tmp;
   img.copyTo(tmp);
   for (int i=0; i<positives.size(); i++) 
      rectangle(tmp,positives[i].getSelection(),Scalar(0,0,255),2);
   if (current!=NULL)
      rectangle(tmp,current->getSelection(),Scalar(0,255,255),2);
   imshow(windowName,tmp);
   tmp.release();
}

Selection* selection=NULL;

void mouseClick(int event, int x, int y, int flags, void* userdata) {
   if (event == EVENT_LBUTTONDOWN) {
      selection->setTopLeft(x,y);
   } else if (event == EVENT_LBUTTONUP || flags==EVENT_FLAG_LBUTTON) {
      selection->setBottomRight(x,y);
   }
}

int main(int argc, char** argv )
{
   int posCounter=300;
   int negCounter=300;

    if (argc < 4) {
       cout << "Not enough parameter" << endl;
       cout << "main -v/-i input_dir output_dir" << endl;
       return 1;
    }

    ofstream finfo((string(argv[3])+string("info.txt")).c_str(),ios::out|ios::app);
    ofstream fbg((string(argv[3])+string("bg.txt")).c_str(),ios::out|ios::app);

    ImageSequence* imgs;
    if (string(argv[1]) == "-v")
       imgs=new ImageSequence(argv[2],true);
    else
       imgs=new ImageSequence(argv[2],false);

    namedWindow(windowName,CV_WINDOW_AUTOSIZE);
    setMouseCallback(windowName,mouseClick,NULL);

    while (imgs->isNextImage()) {
       Mat img=imgs->getNextImage();
       selection=new Selection(img);
       imshow(windowName,selection->getImage());
       char command;
       command=waitKey(10);

       vector<Selection> positives;
       vector<Selection> negatives;
       while (command!='n') {
          command=waitKey(10);
          if (command=='s') {
             imgs->skip(20*30); // 20 sec
             img=imgs->getNextImage();
             delete selection;
             selection=new Selection(img);
          } else if (command=='v') { 
             img.deallocate();
             img=imgs->getNextVideo();
             delete selection;
             selection=new Selection(img);
             imshow(windowName,selection->getImage());
          } else if (command=='f') {
             img.deallocate();
             img=imgs->flipCurrentImage();
             delete selection;
             selection=new Selection(img);
             imshow(windowName,selection->getImage());
          }

          drawMultipleSelections(positives,selection,img);
          if (command=='p' && selection->isValid()) {
             positives.push_back(*selection);
             delete selection;
             selection=new Selection(img);
          } else if (command=='o' && selection->isValid()) {
             negatives.push_back(*selection);
             delete selection;
             selection=new Selection(img);
          } else if (command=='i') {
             positives.clear();
             delete selection;
             selection=new Selection(img);
          }
       } 
       if (positives.size() > 0) {
          for (int i=0; i<positives.size(); i++) {
              Mat positive=img(positives[i].getSelection());
              ostringstream ss;
              ss << posCounter;
              imwrite(string(argv[3])+string("pos")+ss.str()+string(".jpg"),positive);
              finfo << string(argv[3])+string("pos")+ss.str()+string(".jpg") << " 1";
              finfo << " " << 0 << " " << 0;
              finfo << " " << positives[i].getWidthHeight().x << " " << positives[i].getWidthHeight().y;
              finfo << endl;
              posCounter++;
          }
       }
       if (negatives.size() > 0) {
          for (int i=0; i<negatives.size(); i++) {
              Mat negative=img(negatives[i].getSelection());
              ostringstream ss;
              ss << negCounter;
              imwrite(string(argv[3])+string("neg")+ss.str()+string(".jpg"),negative);
              fbg << string(argv[3])+string("neg")+ss.str()+string(".jpg");
              fbg << endl;
              negCounter++;
          }
       }
       delete selection;
    }
    delete imgs;

    return 0;
}

